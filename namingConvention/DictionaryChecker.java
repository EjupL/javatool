package namingConvention;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class DictionaryChecker {

	public Map<Integer, String> createDictionary(
			String dictionaryFilePathLocation) {
		Map<Integer, String> dictionary = new HashMap<Integer, String>();

		try {
			BufferedReader fileReader = new BufferedReader(new FileReader(
					new File(dictionaryFilePathLocation)));
			String line;
			int i = 0;
			while (true) {
				line = fileReader.readLine();
				if (line == null)
					break;

				dictionary.put(i, line);
				i++;
			}

			fileReader.close();
		} catch (IOException ioEx) {
			System.err.println(ioEx.getMessage());
			System.exit(2);
		}

		return dictionary;
	}

}
