package cmd;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.NoHeadException;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;

import sql.SqlCredintials;

public class ExtractCommits extends SqlCredintials {

	private String projectName;
	private String projectURL;
	private List<String> commitId = new ArrayList<String>();
	private Map<String, String> commitIdAndGitDir = new HashMap<String, String>();
	private Map<String, String> commitIdLink = new HashMap<String, String>();

	public String getProjectName() {
		return projectName;
	}

	public String getProjectURL() {
		return projectURL;
	}

	public Map<String, String> getcommitIdAndGitDirMAP() {
		return this.commitIdAndGitDir;
	}

	public Map<String, String> getcommitIdLink() {
		return this.commitIdLink;
	}

	public ExtractCommits(String projectName, String projectURL) {
		this.projectName = projectName;
		this.projectURL = projectURL;
	}

	public List<String> getCommitId() {
		return this.commitId;
	}

	public void extractCommitsForProject(String projectPath, String dateFrom,
			String dateUntil) throws IOException {
		ProcessBuilder builder = new ProcessBuilder("cmd.exe", "/c", "cd \""
				+ projectPath + "\" "
				+ "&& git log --pretty=format:\"%H\" --after=" + dateFrom
				+ " --before=" + dateUntil + "");

		builder.redirectErrorStream(true);
		Process p = builder.start();
		BufferedReader r = new BufferedReader(new InputStreamReader(
				p.getInputStream()));

		String line;
		while (true) {
			line = r.readLine();
			if (line == null)
				break;

			// System.out.println(line);
			this.commitId.add(line.toString());

		}

	}

	public void extractCommitsForFiles(String projectPath, String filePath,
			String dateBefore) throws IOException {
		ProcessBuilder builder = new ProcessBuilder("cmd.exe", "/c", "cd \""
				+ projectPath + "\" "
				+ "&& git log -1 --pretty=format:\"%H\" --before=" + dateBefore
				+ " " + filePath + "");

		builder.redirectErrorStream(true);
		Process p = builder.start();
		BufferedReader r = new BufferedReader(new InputStreamReader(
				p.getInputStream()));
		String line;
		while (true) {
			line = r.readLine();
			if (line == null)
				break;

			System.out.println(line);
			this.commitId.add(line.toString());

		}

	}

	public void extractCommitsFromJIRAProject(String projectName)
			throws SQLException {

		Connection connection = DriverManager.getConnection(url, username,
				password);
		String query = "SELECT * FROM issuesandcommits WHERE `KeyLink` LIKE '%"
				+ projectName + "%'";
		// create the java statement
		Statement st = connection.createStatement();
		ResultSet rs = st.executeQuery(query);
		String link = null;
		while (rs.next()) {
			String key = rs.getString("Key");
			String KeyLink = rs.getString("CommitLink");

			link = KeyLink.split("commits/")[0] + "commits/";
			String commitId = KeyLink.split("commits/")[1];
			String gitDirectory = KeyLink.split("repos/")[1].split("/")[1];
			this.commitId.add(commitId);
			this.commitIdAndGitDir.put(commitId, gitDirectory);
			this.commitIdLink.put(commitId, link);

			System.out.println(gitDirectory + " - " + link + ": " + commitId);

		}

		this.projectURL = link;
		connection.close();
	}

	public Boolean commitExist(String commitId) throws SQLException {

		Connection connection = DriverManager.getConnection(url, username,
				password);
		String query = "SELECT CommitId FROM methodsdetails WHERE CommitId = '"
				+ commitId + "' LIMIT 1";
		// create the java statement
		Statement st = connection.createStatement();
		ResultSet rs = st.executeQuery(query);

		if (rs.next()) {
			System.out.println(commitId);
			connection.close();
			return true;
		} else {
			connection.close();
			return false;
		}

	}
}
