package cmd;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

import lawOfDemeter.AnalyzeMethodsByLawOfDemeter;
import sql.sqlManager;
import extractor.MethodAndCommentExtractor;
import functionHelpers.splitResults;

public class RunCmd {

	public static void runCmdCommand(String projectName, String aPath,
			String filePath, String commitID, Boolean insertQuery)
			throws Exception {
		// The cmd command is executed using the tools JavaMethodExtractor and
		// CommentCounter
		ProcessBuilder builder = new ProcessBuilder("cmd.exe", "/c",
				"D: && cd \"" + aPath + "\" "
						+ "&& java -jar JavaMethodExtractor.jar " + filePath
						+ " ");

		sqlManager sql = new sqlManager();
		Map<String, Integer> comments = new HashMap<String, Integer>();
		Map<String, String> methodValues = new HashMap<String, String>();

		builder.redirectErrorStream(true);
		Process p = builder.start();
		BufferedReader r = new BufferedReader(new InputStreamReader(
				p.getInputStream()));
		String line;

		while (true) {
			line = r.readLine();
			if (line == null) {
				break;
			}

			if (line.startsWith(filePath)) {
				String arrayString[] = line.split("\\s+");
				String results = arrayString[0] + "	" + arrayString[1] + "	"
						+ arrayString[2] + "	" + arrayString[3];

				// System.out.println(results);
				comments = MethodAndCommentExtractor.extractComments(aPath
						+ arrayString[0], Integer.parseInt(arrayString[2]),
						Integer.parseInt(arrayString[3]));

				methodValues = splitResults.splitMethodExtractor(results);
				// String commitId = GitCmd.gitLog(aPath, filePath,
				// Integer.parseInt(methodValues.get("startLine")),
				// Integer.parseInt(methodValues.get("endLine")));

				if (insertQuery == true)
					sql.insertMethods(projectName,
							methodValues.get("methodName"),
							methodValues.get("path"),
							Integer.parseInt(methodValues.get("startLine")),
							Integer.parseInt(methodValues.get("endLine")),
							methodValues.get("methodType"), comments.get("1"),
							comments.get("2"), comments.get("3"),
							comments.get("4"), comments.get("5"),
							comments.get("6"), comments.get("7"), commitID);

				// System.out.println( methodValues.get("startLine") + "	" +
				// methodValues.get("endLine") + "	" +
				// methodValues.get("methodName") + "	" + comments);

			}

			// System.out.println(line);

		}
	}
}