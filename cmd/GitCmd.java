package cmd;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class GitCmd {

	public static String gitLog(String aPath, String filePath, int startLine,
			int EndLine) throws IOException {
		ProcessBuilder builder = new ProcessBuilder("cmd.exe", "/c", "cd \""
				+ aPath + "\" " + "&& git log --graph -u -1  -L" + startLine
				+ "," + EndLine + ":" + filePath + " ");

		builder.redirectErrorStream(true);

		Process p = builder.start();
		BufferedReader r = new BufferedReader(new InputStreamReader(
				p.getInputStream()));
		String line;
		String[] sha = null;
		while (true) {
			line = r.readLine();
			if (line == null)
				break;

			// Display only additions and deletions
			if (line.startsWith("*")) {
				sha = line.split("\\s+");
				// System.out.println(sha[2]);

			}

			/*
			 * if(line.startsWith("| +")) { System.out.println(line); }
			 * if(line.startsWith("| -")) { System.out.println(line); }
			 */

			// System.out.println(line);
		}
		return sha[2];

	}

}
