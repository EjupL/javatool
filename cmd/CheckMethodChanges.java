package cmd;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Array;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Locale;

public class CheckMethodChanges {

	public void helloWorld() {
		/* This method will print "Hello World!" */
		System.out.println("Hellow World!");
		// this is the end of method
	}

	private String filePath;

	public String getFilePath() {
		return filePath;
	}

	public ArrayList<Integer> getLinesThatChanged(String aPath,
			String filePath, String commitId) throws IOException,
			ParseException {
		ProcessBuilder builder = new ProcessBuilder("cmd.exe", "/c", "cd \""
				+ aPath + "\" " + "&& git show  -U0 " + commitId + " "
				+ filePath);
		this.filePath = filePath;
		builder.redirectErrorStream(true);

		Process p = builder.start();
		BufferedReader r = new BufferedReader(new InputStreamReader(
				p.getInputStream()));
		String line;
		String[] sha = null;
		ArrayList<Integer> changes = new ArrayList<Integer>();
		while (true) {
			line = r.readLine();
			if (line == null)
				break;

			// Display only additions and deletions
			if (line.startsWith("@")) {
				String[] linesOfCode = line.split("\\s+");
				// System.out.println(linesOfCode[1] + "  " + linesOfCode[2] );

				String changedLine = linesOfCode[2].replace("+", "");
				Number changedLineToNumber = NumberFormat.getNumberInstance(
						Locale.FRANCE).parse(changedLine);
				double chagedLineToInt = Double.parseDouble(changedLineToNumber
						.toString());
				changes.add((int) Math.floor(chagedLineToInt));

			}
			// if(line.startsWith("+") || line.startsWith("-"))
			// System.out.println(line);

			// System.out.println(line);
		}

		return changes;
	}

}
