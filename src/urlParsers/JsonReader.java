package urlParsers;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;

import org.eclipse.jgit.util.Base64;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JsonReader {

	public static String readUrl(String urlString) throws Exception {
		BufferedReader reader = null;

		try {
			URL url = new URL(urlString);
			URLConnection uc = url.openConnection();
			String userpass = "USERNAME" + ":" + "PASSWORD"; //please provide your username and pass. to make secure requests to GitHub
			String basicAuth = "Basic "
					+ javax.xml.bind.DatatypeConverter
							.printBase64Binary(userpass.getBytes());

			uc.setRequestProperty("Authorization", basicAuth);
			InputStream in = null;
			try {
				in = uc.getInputStream();
			} catch (Exception e) {
				System.out.println(e.getStackTrace());
			}
			if (in == null)
				return null;

			reader = new BufferedReader(new InputStreamReader(in));

			StringBuffer buffer = new StringBuffer();
			int read;
			char[] chars = new char[1024];
			while ((read = reader.read(chars)) != -1)
				buffer.append(chars, 0, read);

			return buffer.toString();
		} finally {
			if (reader != null)
				reader.close();
		}
	}

	public static void extractAdditions(String projectUrl, String commitId)
			throws JSONException, Exception {
		JSONObject json = new JSONObject(readUrl(projectUrl + commitId));

		JSONObject stats = json.getJSONObject("stats");

		JSONArray files = json.getJSONArray("files");

		System.out.print(files.toString());
	}

	public static InputStream getFileForProject(String projectUrl,
			String commitId, String filePath) {
		try {
			JSONObject json = new JSONObject(readUrl(projectUrl + commitId));

			JSONArray files = json.getJSONArray("files");
			URL file = null;
			for (int i = 0; i < files.length(); i++) {
				String rawURL = (String) files.getJSONObject(i).get("raw_url");
				String getFilePath = rawURL.split("/raw/")[1].split("/", 2)[1];
				String currentFilePath = getFilePath.replace("/", "\\\\");
				if (filePath.equals(currentFilePath)) {
					file = new URL(rawURL);
					return file.openStream();
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
