package lawOfDemeter;

import interfaces.InterfaceSQLCredentials;

import java.awt.List;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import sql.SqlCredintials;
import urlParsers.JsonReader;

public class AnalyzeMethodsByLawOfDemeter extends SqlCredintials {

	public static void analyseMethodsForProject(String projectName)
			throws SQLException {
		Connection connection = DriverManager.getConnection(url, username,
				password);

		String query = "SELECT c.projectURL, c.CommitId, m.ID,  m.ProjectName,  m.MethodName,  m.MethodLineStart,  m.MethodLineEnd,  m.Changed,  m.MethodPath,  m.CommitId FROM "
				+ " methodsdetails as m, commitid as c WHERE m.CommitId = c.CommitId AND m.Changed=1 AND m.ProjectName = '"
				+ projectName + "' ";

		Statement st = connection.createStatement();
		ResultSet rs = st.executeQuery(query);
		ArrayList<Integer> methods = methodExist();
		while (rs.next()) {
			int id = rs.getInt("ID");
			String methodName = rs.getString("MethodName");
			int startLine = Integer.parseInt(rs.getString("MethodLineStart"));
			int endLine = Integer.parseInt(rs.getString("MethodLineEnd"));
			String methodPath = rs.getString("MethodPath");
			String commitId = rs.getString("CommitId");
			String projectURL = rs.getString("projectURL");

			if (methods.contains(id)) {
				System.out.println(methodName + " already exist!");
				continue;
			}

			InputStream file = JsonReader.getFileForProject(projectURL,
					commitId, methodPath);

			CheckLawOfDemeter lawOfDemeter = new CheckLawOfDemeter();
			// Tuple will contain the methods with the results of dots and
			// semicolons number appeared inside the method code
			Tuple analyzedMethodResults = lawOfDemeter.analyzeLawOfDemeter(
					startLine, endLine, file);

			if (analyzedMethodResults.getDots() >= 0
					&& analyzedMethodResults.getSemicolons() >= 0) {
				float demeterResult = (float) analyzedMethodResults.getDots()
						/ analyzedMethodResults.getSemicolons();

				System.out.println(methodName + ": " + demeterResult);
				if (!Double.isNaN(demeterResult)
						&& Double.isFinite(demeterResult))
					insertResultsToDatabase(projectName, demeterResult, id,
							connection);
			}
		}
		connection.close();
	}

	public static void analyzeMethodForProject(String projectName,
			int methodId, int startLine, int endLine, InputStream file,
			Connection connection) throws SQLException {
		CheckLawOfDemeter lawOfDemeter = new CheckLawOfDemeter();

		Tuple analyzedMethod = lawOfDemeter.analyzeLawOfDemeter(startLine,
				endLine, file);

		if (analyzedMethod.getDots() > 0 && analyzedMethod.getSemicolons() > 0) {
			float demeterResult = (float) analyzedMethod.getDots()
					/ analyzedMethod.getSemicolons();

			System.out.println("Result: " + demeterResult);

			insertResultsToDatabase(projectName, demeterResult, methodId,
					connection);
		}

	}

	private static void insertResultsToDatabase(String projectName,
			float demeterResult, int methodId, Connection connection)
			throws SQLException {
		String insertQuery = "INSERT INTO law_of_demeter "
				+ "(ProjectName, LawOfDemeter, MethodId) " + "VALUES (?, ?, ?)";

		PreparedStatement ps = connection.prepareStatement(insertQuery);

		ps.setString(1, projectName);
		ps.setFloat(2, demeterResult);
		ps.setInt(3, methodId);

		ps.executeUpdate();
	}

	private static ArrayList<Integer> methodExist() throws SQLException {
		Connection connection = DriverManager.getConnection(url, username,
				password);
		String query = "SELECT MethodId FROM law_of_demeter ";
		// create the java statement
		Statement st = connection.createStatement();
		ResultSet rs = st.executeQuery(query);
		ArrayList<Integer> methods = new ArrayList<Integer>();

		while (rs.next()) {
			int methodId = rs.getInt("MethodId");
			methods.add(methodId);
		}
		return methods;
	}
}
