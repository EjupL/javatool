package org.computer.aman.metrics.util;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * ソースコード�?�内容を表�?��?�コードマップ
 * <p>
 * コードマップ�?�，ソースコード�?�内容を文字�?��?�?�表�?��?��?�マップ�?��?�る．<br>
 * 例�?��?�，空白類を 0，何ら�?��?�コードを 1, コメントを 2 �?��?��?�，<br>
 * <pre>   public int x; // 座標</pre>
 * �?�
 * <pre>0001111110111011022022</pre>
 * �?�表�?��?�れる．
 * <p>
 * 本クラス�?�インスタンス�?��?�，
 * 一行�?��?��?�コードマップオブジェクト（CodeLineMap インスタンス）をリスト�?��?��?��?�?�?�る．
 * <p>
 * 具体的�?��?��?��?��?��?�値�?�マップ�?�使�?れる�?��?��?�，CodeLineMap �?�サブクラス（言語�?��?��?�特化�?�れ�?�クラス�?�作られる）�?��?存�?�る．
 * 
 * @author Hirohisa AMAN &lt;aman@computer.org&gt;
 */
public abstract class CodeMap
{    
    /**
     * 指定�?�れ�?�文字列�?�実行コード�?�一部�?��?�る�?��?��?��?�を判定
     * 
     * @param aLine 判定対象文字列
     * @return コード�?�一部�?�ら�?� true �?�も�?��??�?� false
     */
    public abstract boolean isCommentOut(final String aLine);
    
    /**
     * 行�?��?��?�コードマップをイテレータ�?��?��?�返�?�
     * 
     * @return 行�?��?��?�コードマップ�?�対�?�るイテレータ
     */
    public Iterator<CodeLineMap> iterator()
    {
        return lines.iterator();
    }
    
    /**
     * コードマップ�?�内容を文字列�?��?��?��?��?�返�?�．
     * 
     * @return 文字列化�?�れ�?�コードマップ
     */
    public String toString()
    {
        StringBuffer buf = new StringBuffer();
        int lineNumber = 1;
        for (Iterator<CodeLineMap> iterator = lines.iterator(); iterator.hasNext();) {
            CodeLineMap element = iterator.next();
            buf.append(lineNumber++);
            buf.append(element.toString());
            buf.append("\n");
        }
        
        return new String(buf);
    }
    /**
     * CodeLineMap インスタンスをコードマップ�?�末尾�?�追加�?�る
     * 
     * @param aLineMap コードマップ�?�末尾�?�追加�?�れる CodeLineMap インスタンス
     */
    protected void add(final CodeLineMap aLineMap)
    {
        if ( lines == null ){
            lines = new ArrayList<CodeLineMap>();
        }
        lines.add(aLineMap);
    }
      
    /** 
     * コードマップ�?�内容:
     * 行�?��?�?��?�コードマップ�?�相当�?�る CodeLineMap オブジェクトを
     * 順�?付�??リスト�?��?��?��?�?
     */
    private ArrayList<CodeLineMap> lines;
}
