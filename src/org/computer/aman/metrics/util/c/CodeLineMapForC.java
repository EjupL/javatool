package org.computer.aman.metrics.util.c;

import org.computer.aman.metrics.util.CodeLineMap;

/**
 * C ソースコード一行分�?�コードマップを管�?��?�るクラス�?��?�る．
 * <p>
 * コードマップ�?��?�，ソースコード�?�内容�?��?�る�?�文字を<br>
 * (1)コード�?�一部，(2)空白類，(3)コメント文�?�一部，<br>
 * �?��?��?�れ�?��?�分類�?�，�?�ら�?��?��?定�?られ�?�整数値�?��??�?��?列を表�?��?�も�?��?��?�る．
 * <p>
 * (1) �?�定数 CODE，(2) �?�定数 BLANK �?��??れ�?�れ表�?�れる．<br>
 * (3) �?��?��?��?��?�，// �?�ら行末�?��?��?�コメント�?�定数 EOL_COMMENT �?�，
 * C 標準形�?�?�コメント�?�定数 TRADITIONAL_COMMENT �??れ�?�れ表�?�れる．
 * �?��?�，実行コードをコメントアウト�?��?��?��?�?れる場�?��?�，�??�?�形�?�?��?��?�?��?�
 * EOL_COMMENT_OUT �?��?��?� TRADITIONAL_COMMENT_OUT �?�表�?�れる．
 * 
 * @author Hirohisa AMAN &lt;aman@computer.org&gt;
 */
public class CodeLineMapForC 
extends CodeLineMap
{

    /** マップ上�?�，�??�?�文字�?� // 形�?�?�コメント�?��?�る�?��?�を表�?�値 */
    public static final String EOL_COMMENT = "2";
    
    /** マップ上�?�，�??�?�文字�?� // 形�?�?�コメント�?��?�り，�?��?��?��?�，実行コード�?�コメントアウト�?��?�る�?��?�を表�?�値 */
    public static final String EOL_COMMENT_OUT = "5";
    
    /** マップ上�?�，�??�?�文字�?� C 標準形�?�?�コメント�?��?�る�?��?�を表�?�値 */
    public static final String TRADITIONAL_COMMENT = "3";
    
    /** マップ上�?�，�??�?�文字�?� C 標準形�?�?�コメント�?��?�り，�?��?��?��?�，実行コード�?�コメントアウト�?��?�る�?��?�を表�?�値 */
    public static final String TRADITIONAL_COMMENT_OUT = "6";
    
    /**
     * 一行分�?�コードマップを設定�?�る． 
     * @param aMap 一行分�?�コードマップ
     */
    public CodeLineMapForC(final String aMap)
    {
        super(aMap);
        
        for (int i = 0; i < aMap.length(); i++ ){
            String ithChar = Character.toString(aMap.charAt(i));
            if ( EOL_COMMENT.equals(ithChar) ){
                eolCommentCount++;
            }
            else if ( TRADITIONAL_COMMENT.equals(ithChar) ){
                traditionalCommentCount++;
            }
            else if ( EOL_COMMENT_OUT.equals(ithChar) ){
                eolCommentOutCount++;
            }
            else if ( TRADITIONAL_COMMENT_OUT.equals(ithChar) ){
                traditionalCommentOutCount++;
            }
        }
    }

    /**
     * EOL 形�?�?�コメント�?�文字数を返�?�．
     *
     * @return EOL 形�?�?�コメント�?�文字数
     */
    public int getEolCommentCount()
    {
        return eolCommentCount;
    }
    
    /**
     * EOL 形�?�?�コメントアウト�?�文字数を返�?�．
     *
     * @return EOL 形�?�?�コメントアウト�?�文字数
     */
    public int getEolCommentOutCount()
    {
        return eolCommentOutCount;
    }

    /**
     * C 標準形�?�?�コメント�?�文字数を返�?�．
     *
     * @return C 標準形�?�?�コメント�?�文字数
     */
    public int getTraditionalCommentCount()
    {
        return traditionalCommentCount;
    }

    /**
     * C 標準形�?�?�コメントアウト�?�文字数を返�?�．
     *
     * @return C 標準形�?�?�コメントアウト�?�文字数
     */
    public int getTraditionalCommentOutCount()
    {
        return traditionalCommentOutCount;
    }

    /**
     * 一行分�?�コードマップ�?�内容（内訳�?�カウント付�??）を文字列�?��?��?�返�?�．
     * <br>
     * 内容�?� (b?,c?,#?:t?,e?,j?,ec?,tc?) ....... 
     * �?��?��?�形�?�?��?��?��?��?�り，
     * �??れ�?�れ順番�?�空白類�?�文字数，コード文字数，コメント文字数，Traditional コメント文字数，EOL コメント文字数，Javadoc 文字数（未対応�?��?��?0），
     * EOL コメントアウト文字数，Traditional コメントアウト文字数�?� ? �?�入り，�??�?�後�?�コードマップ�?�内容�?�続�??．
     * <br>
     * �?��?�，コメント文字数�?�，�?��?��?��?�タイプ�?�コメント�?��?文字数�?��?�る．
     * @return 一行分�?�コードマップ�?�内容
     */
    public String toString()
    {
        StringBuffer buf = new StringBuffer();        
        buf.append("(b" + getBlankCount() + ",c" + getCodeCount() + ",#" + getCommentCount() + ":");
        buf.append("t" + getTraditionalCommentCount() + ",");
        buf.append("e" + getEolCommentCount() + ",");
        buf.append("j" + 0 + ",");
        buf.append("ec" + getEolCommentOutCount() + ",");
        buf.append("tc" + getTraditionalCommentOutCount() + ")");
        buf.append(" ");
        buf.append(getMap());
        
        return new String(buf);
    }

    /** EOL 形�?�?�コメント�?�文字数 */
    private int eolCommentCount;
    
    /** EOL 形�?�?�コメントアウト�?�文字数 */
    private int eolCommentOutCount;

    /** C 形�?�?�コメント�?�文字数 */
    private int traditionalCommentCount;

    /** C 形�?�?�コメントアウト�?�文字数 */
    private int traditionalCommentOutCount;
}
