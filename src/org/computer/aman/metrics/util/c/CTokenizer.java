package org.computer.aman.metrics.util.c;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class CTokenizer
{
    public CTokenizer(final String aCodeFragment)
    {
        codeFragment = aCodeFragment;
        if ( codeFragment == null || codeFragment.equals("") ){
            tokenList = new LinkedList<String>();
        }
    }
        
    public List<String> getTokenList()
    {
        if ( tokenList == null ){
            tokenList = makeTokenList(codeFragment);
        }        
        return tokenList;
    }
        
    private LinkedList<String> makeTokenList(final String aCodeFragment)
    {
        // �?��?�，コード断片�?�ら文字列リテラル，文字リテラルを見�?��?�出�?�，
        // 文字列（文字）リテラル�?��??れ以外�?��?��?�カテゴリ�?�分割�?�る
        LinkedList<String> tmpList = splitLiterals(aCodeFragment);
        
        // 文字列（文字）リテラル�?��??�?��?��?� tokenList �?�追加�?�，
        // �??れ以外�?��?�ら�?�トークン分�?��?��?��?�ら tokenList �?�追加�?�る
        tokenList = new LinkedList<String>();
        for (Iterator<String> iterator = tmpList.iterator(); iterator.hasNext();) {
            String element = iterator.next();
            if ( element.startsWith("\"") || element.startsWith("'") ){
                tokenList.add(element);
            }
            else{
                tokenList.addAll(makeSubTokenList(element));
            }
        }
        
        return tokenList;
    }
    
    /**
     * 文字リテラル�?文字列リテラル以外�?�文字列�?�対�?��?�トークン分�?�を行�?�，
     * �?果を LinkedList �?��?��?�返�?�．
     * 
     * @param anElement トークン分�?��?�対象（文字リテラル�?文字列リテラル以外�?�文字列）
     * @return トークン分�?��?��?果�?��?��?�得られるトークンリスト
     */
    private LinkedList<String> makeSubTokenList(final String anElement)
    {
        // �?��?�，スペース�?�分断�?��?�仮�?�トークンリストを作り，
        // �??れ�?�れ�?�仮トークン�?�中�?�演算�?�?�登場�?��?��?�る場�?��?�，�?�ら�?��??�?�仮トークンを分断�?�る．
        // �??�?�よ�?��?�分断を�?��?��?��?�対�?��?�施�?�．
        // 一�?��?�仮トークン�?�中�?�複数�?�演算�?�?�登場�?�る�?��?�も�?�る�?��?�，繰り返�?�施�?�必�?�?��?�る．
        String[] tmpTokenArray = anElement.split("\\s");
        LinkedList<String> tmpTokenList = new LinkedList<String>();
        for (int i = 0; i < tmpTokenArray.length; i++) {
            if ( tmpTokenArray[i].length() == 0 ){
                continue;
            }
            tmpTokenList.add(tmpTokenArray[i]);
        }
        int index = 0;
        while ( index < tmpTokenList.size() ){
            // 演算�?（�?��?��?�セパレータ）�?�分断�?�る（splitWithSymbol）
            //  --> �?果�?�，{ 演算�?より�?，演算�?，演算�?より後 } �?��?��?�長�?� 3 �?��?列
            //  �?��?�，演算�?�?�見�?��?�ら�?��?�場�?��?�後�?�?� 2 �?素�?� null �?��?�る
            String[] subTokens = splitWithSymbol(tmpTokenList.get(index));
            if ( subTokens[1] == null ){
                index++;
            }
            else{
                tmpTokenList.remove(index);
                if ( subTokens[0].length() > 0 ){
                    tmpTokenList.add(index, subTokens[0]);
                    index++;
                }
                tmpTokenList.add(index, subTokens[1]);
                index++;
                if ( subTokens[2].length() > 0 ){
                    tmpTokenList.add(index, subTokens[2]);
                }
            }
        }
        
        // 浮動�?数点数リテラル�?�複数�?�トークン�?�分断�?�れ�?��?�る�?�能性も�?�る�?��?
        // リストを順番�?��?ェック�?�，必�?�?�ら�?��?�?��?��?�る
        // �?��?��?�，厳密�?�構文�?ェック�?��?�無�??，ピリオド�?�登場�?��?�場�?��?�，
        // �??�?��?�?�トークン�?�数字�?�始�?��?��?��?��?�らピリオド�?��??�?��?��?�る．
        // �?�様�?�ピリオド�?�後�?�?�数字�?�始�?��?��?��?��?�ら�??�?�ら�?�も�??�?��?��?�る．
        // 
        // �?�ら�?�特殊�?�パターン�?��?��?� 1.2e-3f �?��?��?��?�指数表記も�?�る．
        // �?��?�場�?�，上�?�処�?��?�終�?�?��?�後�?�も，�?��?� - �?�区切られ�?��?�る状態�?��?��?�
        // 符�?�(+-)�?��?�?�トークン�?�「数字�?��?��?�ピリオド�?�始�?��?��?��?��?�，�?��?� e �?�終�?る�?�?��?�り，
        // 後�?�?�トークン�?�「数字�?�始�?�る�?�?�ら�?�三者を�??�?��?��?�る
        index = 0;
        while ( index < tmpTokenList.size() ){
            boolean isReplaced = false;
            if ( tmpTokenList.get(index).equals(".") ){
                String replace = ".";
                boolean removeLeft = false;
                boolean removeRight = false;
                if ( index > 0 && Character.isDigit(tmpTokenList.get(index-1).charAt(0)) ){
                    replace = tmpTokenList.get(index-1) + ".";
                    removeLeft = true;
                }
                if ( index+1 < tmpTokenList.size() && Character.isDigit(tmpTokenList.get(index+1).charAt(0)) ){
                    replace += tmpTokenList.get(index+1);
                    removeRight = true;
                }
                
                if ( removeLeft && removeRight ){
                    tmpTokenList.remove(index+1);
                    tmpTokenList.remove(index);
                    tmpTokenList.remove(index-1);
                    tmpTokenList.add(index-1, replace);
                    isReplaced = true;
                }
                else if ( removeLeft ){
                    tmpTokenList.remove(index);
                    tmpTokenList.remove(index-1);
                    tmpTokenList.add(index-1, replace);                            
                    isReplaced = true;
                }
                else if ( removeRight ){
                    tmpTokenList.remove(index+1);
                    tmpTokenList.remove(index);
                    tmpTokenList.add(index, replace);                            
                    index++;
                    isReplaced = true;
                }
            }
            if ( !isReplaced ){
                index++;
            }
        }
        
        index = 0;
        while ( index < tmpTokenList.size() ){
            boolean isReplaced = false;
            if ( tmpTokenList.get(index).equals("+") || tmpTokenList.get(index).equals("-") ){
                if ( index > 0 && index+1 < tmpTokenList.size() ){
                    char headLeft = tmpTokenList.get(index-1).charAt(0);
                    char tailLeft = tmpTokenList.get(index-1).charAt(tmpTokenList.get(index-1).length()-1);
                    char headRight = tmpTokenList.get(index+1).charAt(0);
                    if ( (Character.isDigit(headLeft) || headRight == '.') 
                            && (tailLeft == 'e' || tailLeft == 'E')
                            && Character.isDigit(headRight) ){
                        String replace = tmpTokenList.get(index-1) + tmpTokenList.get(index) + tmpTokenList.get(index+1);
                        tmpTokenList.remove(index+1);
                        tmpTokenList.remove(index);
                        tmpTokenList.remove(index-1);
                        tmpTokenList.add(index-1, replace);
                        isReplaced = true;
                    }
                }
            }
            if ( !isReplaced ){
                index++;
            }
        }
        
        return tmpTokenList;
    }
    
    
    /**
     * コード断片を演算�?（�?��?��?�区切り文字）�?�分断�?�る
     * 
     * @param aCodeFragment 分断対象�?�コード断片
     * @return String �?列 { 演算�?より左，演算�?，演算�?より�?� }
     */
    private String[] splitWithSymbol(final String aCodeFragment)
    {
        final String[] SYMBOLS
            = { ">>=", "<<=", "&=", "|=",  "^=",
                "::", "++", "--", "->", "<<", ">>", "==", "!=", "&&", "||", "+=", "-=", "*=", "/=", "%=", "<=", ">=",
                "!", "~", "+", "-", "(", ")", ".", "*", "&", "[", "]", "^", "|", "?", ":", "=", ",", ">", "<", "/", "%", "{", "}",  ";"
              };
        // 最も左�?�登場�?�る演算�?・区切り文字�?�分断�?�る
        int cutPoint = aCodeFragment.length();
        int symbol = -1;
        for ( int i = 0; i < SYMBOLS.length; i++ ){
            int index = aCodeFragment.indexOf(SYMBOLS[i]);
            if ( index != -1 ){
                if ( index < cutPoint ){
                    cutPoint = index;
                    symbol = i;
                }
            }
        }
        
        if ( symbol != -1 ){
            return new String[]{ aCodeFragment.substring(0, cutPoint), SYMBOLS[symbol], aCodeFragment.substring(cutPoint+SYMBOLS[symbol].length()) };
        }
        
        return new String[]{ aCodeFragment, null, null };
    }
    
    /**
     * コード断片�?�ら文字列リテラル，文字リテラルを見�?��?�出�?�，
     * 文字列（文字）リテラル�?��??れ以外�?��?��?�カテゴリ�?�分割�?�る
     * 
     * @return 分断後�?�コード断片を格�?�?��?�リスト
     */
    private LinkedList<String> splitLiterals(final String aCodeFragment)
    {
        // codeFragment �?�先頭�?�ら一文字�?��?��?ェック�?��?�，状態�?�移を確�?�?��?��?�ら
        // コード断片を分断�?��?��?��??（分断後�?�コード断片を tmpList �?�入れ�?��?��??）
        // [状態] status
        // 0 : �?期状態
        // 1 : 文字列リテラル�?�中
        // 11 : 文字列リテラル中�?� \ �?�よるエスケープ文字を読込ん�?�直後�?�状態
        // 2 : 文字リテラル�?�中
        // 21 : 文字リテラル中�?� \ �?�よるエスケープ文字を読込ん�?�直後�?�状態
        // 3 : 文字列リテラル�?�も文字リテラル�?�も�?��?�状態
        // 31 : \ �?�よるエスケープ文字を読込ん�?�直後�?�状態
        LinkedList<String> tmpList = new LinkedList<String>();
        int status = 0;
        StringBuilder tmpString = new StringBuilder();
        for ( int i = 0; i < aCodeFragment.length(); i++ ){
            char ch = aCodeFragment.charAt(i);
            switch ( status ){
            case 0 : // 0 : �?期状態
                tmpString.append(ch);
                if ( ch == '"' ){
                    status = 1;
                }
                else if ( ch == '\''){
                    status = 2;
                }
                else{
                    status = 3;
                }
                break;
            case 1 : // 1 : 文字列リテラル�?�中
                tmpString.append(ch);
                if ( ch == '\\' ){
                    status = 11;
                }
                else if ( ch == '"' ){
                    status = 0;
                    tmpList.add(new String(tmpString));
                    tmpString.delete(0, tmpString.length());
                }
                break;
            case 11 : // 11 : 文字列リテラル中�?� \ �?�よるエスケープ文字を読込ん�?�直後�?�状態
                tmpString.append(ch);
                status = 1;
                break;
            case 2 : // 2 : 文字リテラル�?�中
                tmpString.append(ch);
                if ( ch == '\\' ){
                    status = 21;
                }
                else if ( ch == '\'' ){
                    status = 0;
                    tmpList.add(new String(tmpString));
                    tmpString.delete(0, tmpString.length());
                }
                break;
            case 21 : // 21 : 文字リテラル中�?� \ �?�よるエスケープ文字を読込ん�?�直後�?�状態
                tmpString.append(ch);
                status = 2;
                break;
            case 3 : // 3 : 文字列リテラル�?�も文字リテラル�?�も�?��?�状態
                if ( ch == '"' ){
                    status = 1;
                    tmpList.add(new String(tmpString));
                    tmpString.delete(0, tmpString.length());
                    tmpString.append(ch);
                }
                else if ( ch == '\'' ){
                    status = 2;
                    tmpList.add(new String(tmpString));
                    tmpString.delete(0, tmpString.length());
                    tmpString.append(ch);
                }
                else if ( ch == '\\' ){
                    status = 31;
                    tmpString.append(ch);
                }
                else{
                    tmpString.append(ch);
                }
                break;
            case 31 : // 31 : \ �?�よるエスケープ文字を読込ん�?�直後�?�状態
                tmpString.append(ch);
                status = 3;
                break;                
            }
        }
        if ( tmpString.length() > 0 ){
            if ( status == 1 || status == 2 ){
                // 文字リテラル�?��?��?�文字列リテラル�?�状態�?��?��?�
                // コード断片�?�終端�?�到�?��?��?��?�る場�?�，�?完全�?�リテラル�?��?��?�
                // �?��?��?��?�る�?��?，先頭�?�一文字�?��?�をトークン�?��?��?�，
                // 二文字目以�?�?��?��?�メソッドを�?帰的�?�利用�?�る
                tmpList.add(tmpString.substring(0,1));
                tmpString.delete(0, 1);
                tmpList.addAll(splitLiterals(new String(tmpString)));
            }
            else{                
                tmpList.add(new String(tmpString));
            }
        }
        
        return tmpList;
    }
    
    /** トークン分�?�対象�?�文字列 */
    private String codeFragment;
    
    /** トークン分�?��?��?果 */
    private LinkedList<String> tokenList;
}
