package org.computer.aman.metrics.util.java;

import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.computer.aman.io.sourcecode.SourceFile;
import org.computer.aman.metrics.util.CodeLineMap;
import org.computer.aman.metrics.util.CodeMap;

/**
 * Java ソースコード�?�内容を表�?��?�コードマップ
 * <p>
 * コードマップ�?�，ソースコード�?�内容を文字�?��?�?�表�?��?��?�マップ�?��?�る．<br>
 * �?��?��?��?�，
 * <ul>
 *  <li> 空白類: 0
 *  <li> 何ら�?��?�コード: 1
 *  <li> // コメント: 2
 *  <li> /* コメント: 3
 *  <li> Javadoc: 4
 * </ul>
 * �?�使�?れる．
 * <p>
 * 本クラス�?�インスタンス�?��?�，
 * 一行�?��?��?�コードマップオブジェクト（CodeLineMapForJava インスタンス）をリスト�?��?��?��?�?�?�る．
 * 
 * @author Hirohisa AMAN &lt;aman@computer.org&gt;
 */
public class CodeMapForJava 
extends CodeMap
{
    /**
     * Java ソースファイル�?�対応�?��?� CodeMap オブジェクトを生�?�?�る
     * 
     * @param aSourceFile ソースファイル
     * @throws NotSupportedSourceFileExeption ソースファイル�?� Java ソース�?��?��?�場�?�
     * @throws IOException 入出力�?�何ら�?��?�例外�?�発生�?��?�場�?�
     */
    public CodeMapForJava(final SourceFile aSourceFile) 
    throws IOException
    {
        // 1 行�?��?�読�?�込�?�，�??�?�内容�?��?��?��?�調�?��?��?�ら CodeLineMap を CodeMap �?�追加�?��?��?��??
        //
        // status - コード解�?�?�状態
        //   1 : 何ら�?��?�コード（�?�コメント，�?�文字（列）リテラル）を読�?�出�?�中
        //   12: Traditional コメント（/* ... ）
        //   13: Javadoc (/** ... )
        //   100: 文字列リテラル�?�内部（"..."）
        //   200: 文字リテラル�?�内部（'.'）
        final int CODE = 1;
        final int TRADITIONAL = 12;
        final int JAVADOC = 13;
        final int STRING_LITERAL = 100;
        final int CHAR_LITERAL = 200;
        int status = CODE;

        LineNumberReader reader = new LineNumberReader(new FileReader(aSourceFile));
        LinkedList<String> mapList = new LinkedList<String>();
        StringBuilder map = new StringBuilder(); // CodeLineMap を作り上�?�る�?��?�?�作業用 StringBuilder
        StringBuilder contents = new StringBuilder(); // Traditional コメント形�?�?�よるコメントアウトを判定�?�る�?��?，コメント内容�?�一時�?存�?�使�?�
        
        String line = null;
        while ( (line = reader.readLine()) != null ){
            // ソースコード行�?�ら 1 文字�?��?��?�り出�?��?�調�?�，状態を�?�移�?��?��?��?��??
            int idx = 0;
            while ( idx < line.length() ){    
                char ch = line.charAt(idx);
                
                if ( Character.isWhitespace(ch) ){ 
                    // 空白類�?�場�?�，�??�?��?��?�をマップ�?�記録�?��?�以�?�?�スキップ�?�る
                    // �?��?��?�，コメント�?�内部�?�ら�?�コメント�?�一部�?��?��?�，文字列リテラル�?�ら�?�コード�?�一部�?��?��?�処�?��?�る
                    // �?��?�，Traditional コメント�?�関�?��?��?�，コメントアウト�?��?�能性も�?�る�?��?，contents �?�も�?��?�?��?��??
                    if ( status == TRADITIONAL ){ // Traditional コメント�?�場�?� 
                        map.append(CodeLineMapForJava.TRADITIONAL_COMMENT);
                        contents.append(ch);
                    }
                    else if ( status == STRING_LITERAL || status == CHAR_LITERAL ){ // 文字（列）リテラル�?�場�?�
                        map.append(CodeLineMap.CODE);
                    }
                    else if ( status == JAVADOC ){ // Javadoc �?�場�?�
                        map.append(CodeLineMapForJava.JAVADOC_COMMENT);
                    }
                    else{                            
                        map.append(CodeLineMap.BLANK);
                    }
                    idx++;
                    continue;
                }

                ///////////////////////////////////////////////////////////////
                // 以下，�?�り出�?��?�文字 ch �?�応�?��?�状態 status を変化�?��?��?��?��??
                // �?��?�，必�?�?�応�?��?�文字�?置 idx も更新�?��?��?��??
                
                // 通常�?�コード状態(CODE) �?�場�?�:
                if ( status == CODE ){ 
                    if ( ch == '/' ){ // コメント開始�?��?�能性: 先読�?��?��?�決�?る
                        // 次�?�続�??文字�?� '/' �?�ら EOL コメント�?�確定,
                        // '**' �?�ら Javadoc, '*' �?�ら Traditional コメント,
                        // �?��?�れ�?�も�?��?��?�ら�?�通常�?�コード�?�一部
                        String remainedPart = line.substring(idx);
                        if ( remainedPart.startsWith("//") ){ // EOL コメント�?��?��?�行末�?��?�を処�?�
                            // �?��?��?�，コード�?�コメントアウト�?�場�?��?��??�?�ら�?�切り替�?�る
                            idx += remainedPart.length();
                            appendCodeToMap(map, isCommentOut(trimComment(remainedPart)) ? CodeLineMapForJava.EOL_COMMENT_OUT : CodeLineMapForJava.EOL_COMMENT, remainedPart.length());
                        }
                        else if ( remainedPart.startsWith("/**") ){ // Javadoc �?��?��?�状態�?�移
                            status = JAVADOC;
                            idx += "/**".length();
                            appendCodeToMap(map, CodeLineMapForJava.JAVADOC_COMMENT, "/**".length());
                        }
                        else if ( remainedPart.startsWith("/*") ){ // Traditional コメント�?��?��?�状態�?�移
                            status = TRADITIONAL;
                            idx += "/*".length();
                            appendCodeToMap(map, CodeLineMapForJava.TRADITIONAL_COMMENT, "/*".length());
                            contents.delete(0, contents.length());
                            contents.append("/*");
                        }
                        else{
                            idx++;
                            appendCodeToMap(map, CodeLineMapForJava.CODE, 1);
                        }
                    }
                    else{
                        // 文字（列）リテラル�?�場�?��?��?�状態�?�移を起�?��?�
                        if ( ch == '\''){ // 文字リテラル�?�開始
                            status = CHAR_LITERAL;
                        }
                        else if ( ch == '"' ){ // 文字列リテラル�?�開始
                            status = STRING_LITERAL;
                        }                        
                        idx++;
                        appendCodeToMap(map, CodeLineMapForJava.CODE, 1);          
                    }
                    continue;
                }
                
                // Traditional コメント状態(TRADITIONAL) �?�場�?�:
                if ( status == TRADITIONAL ){
                    if ( ch == '*' && idx+1 < line.length() && line.charAt(idx+1) == '/' ){
                        idx += "*/".length();
                        appendCodeToMap(map, CodeLineMapForJava.TRADITIONAL_COMMENT, "*/".length());
                        contents.append("*/");
                        status = CODE;
                        if ( isCommentOut(trimComment(new String(contents))) ){
                            // マップを�?�り，最後�?�連続�?��?� TRADITIONAL_COMMENT �?�列を 
                            // TRADITIONAL_COMMENT_OUT �?�列�?�塗り替る
                            int length = contents.length();
                            for ( int i = map.length()-1; i >= 0; i-- ){
                                map.replace(i, i+1, CodeLineMapForJava.TRADITIONAL_COMMENT_OUT);
                                length--;
                                if ( length == 0 ){
                                    break;
                                }
                            }
                            LinkedList<String> replacedMapList = new LinkedList<String>();
                            while ( length > 0 ){
                                StringBuilder buf = new StringBuilder(mapList.removeLast());
                                for ( int i = buf.length()-1; i >= 0; i-- ){
                                    buf.replace(i, i+1, CodeLineMapForJava.TRADITIONAL_COMMENT_OUT);
                                    length--;
                                    if ( length == 0 ){
                                        break;
                                    }
                                }
                                replacedMapList.addLast(new String(buf));
                            }
                            if ( replacedMapList.size() > 0 ){
                                mapList.addAll(replacedMapList);
                            }
                        }
                    }
                    else{
                        idx++;
                        appendCodeToMap(map, CodeLineMapForJava.TRADITIONAL_COMMENT, 1);
                        contents.append(ch);
                    }
                    continue;
                }
                
                // Javadoc 状態(JAVADOC) �?�場�?�:
                if ( status == JAVADOC ){
                    if ( ch == '*' && idx+1 < line.length() && line.charAt(idx+1) == '/' ){
                        idx += "*/".length();
                        appendCodeToMap(map, CodeLineMapForJava.JAVADOC_COMMENT, "*/".length());
                        status = CODE;
                    }
                    else{
                        idx++;
                        appendCodeToMap(map, CodeLineMapForJava.JAVADOC_COMMENT, 1);
                    }
                    continue;
                }
                
                // 文字リテラル状態(CHAR_LITERAL) �?�場�?�:
                if ( status == CHAR_LITERAL ){
                    idx++;
                    appendCodeToMap(map, CodeLineMapForJava.CODE, 1);
                    if ( ch == '\\' ){ // エスケープ文字�?�場�?��?�無�?�件�?�次�?�文字をリテラル�?��?�る
                        idx++;
                        appendCodeToMap(map, CodeLineMapForJava.CODE, 1);
                    }
                    else if ( ch == '\'' ){
                        status = CODE;
                    }
                    continue;
                }
                
                // 文字列リテラル状態(STRING_LITERAL) �?�場�?�:
                if ( status == STRING_LITERAL ){
                    idx++;
                    appendCodeToMap(map, CodeLineMapForJava.CODE, 1);
                    if ( ch == '\\' ){ // エスケープ文字�?�場�?��?�無�?�件�?�次�?�文字をリテラル�?��?�る
                        idx++;
                        appendCodeToMap(map, CodeLineMapForJava.CODE, 1);
                    }
                    else if ( ch == '"' ){
                        status = CODE;
                    }
                    continue;
                }
            }

            // 行�?�解�?�?�終�?�?��?�ら，�??�?��?果を CodeLineMapForJava オブジェクト�?��?��?�構築�?�，リスト�?�追加�?��?��?��??
            mapList.add(new String(map));
            map.delete(0, map.length());
        }

        for (Iterator<String> iterator = mapList.iterator(); iterator.hasNext();) {
            add(new CodeLineMapForJava(iterator.next()));            
        }

        reader.close();
    }    

    /**
     * 指定�?�れ�?�マップ（StringBuilder）�?�末尾を aContent �?�
     * aCount 文字分�?��?�塗る
     * 
     * @param aMap 対象マップ
     * @param aContent 塗る内容
     * @param aCount 塗る回数（文字数）
     */
    private void appendCodeToMap(StringBuilder aMap, final String aContent, final int aCount)
    {
        for ( int j = 0; j < aCount; j++ ){
            aMap.append(aContent);
        }
    }
    
    /**
     * 指定�?�れ�?�コメント文�?�コメント開始文字列�?�終了文字列を�?�り除�?��?�文字列を返�?�
     * 
     * @param aComment コメント文
     * @return コメント開始文字列�?�終了文字列を�?�り除�?��?�文字列
     */
    private String trimComment(final String aComment)
    {
        if ( aComment.startsWith("//") ){
            return aComment.substring(2);
        }
        if ( aComment.startsWith("/*") ){
            return aComment.substring(2, aComment.length()-2);
        }
        return null;
    }
    
    /**
     * 指定�?�れ�?�文字列�?�実行コード�?�一部�?��?�る�?��?��?��?�を判定
     * 
     * @param aFragment 判定対象文字列
     * @return コード�?�一部�?�ら�?� true �?�も�?��??�?� false
     */
    public boolean isCommentOut(final String aFragment)
    {
        // 文字列をトークン分�?��?�，�??�?�末尾トークン�?� {, } �?��?��?�セミコロン
        // �?�ら�?�コード�?�一部�?�見�?��?�
        List<String> list = (new JavaTokenizer(aFragment)).getTokenList();
        if ( list.size() == 0 ){
            return false;
        }
        String tail = list.get(list.size()-1);
        if ( tail.equals("{") || tail.equals("}") || tail.equals(";") ){
            return true;
        }

        return false;
    }
}
