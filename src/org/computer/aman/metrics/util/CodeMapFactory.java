package org.computer.aman.metrics.util;

import java.io.IOException;

import org.computer.aman.io.sourcecode.NotSupportedSourceFileExeption;
import org.computer.aman.io.sourcecode.SourceFile;
import org.computer.aman.metrics.util.c.CodeMapForC;
import org.computer.aman.metrics.util.java.CodeMapForJava;

/**
 * ソースファイル�?�拡張�?（プログラミング言語）�?�応�?��?��?�切�?� CodeMap オブジェクトを生�?�?�るファクトリ
 * 
 * @author Hirohisa AMAN &lt;aman@computer.org&gt;
 */
public class CodeMapFactory
{
    /**
     * ソースファイル�?�拡張�?（プログラミング言語）�?�応�?��?��?�切�?� CodeMap オブジェクトを生�?�?�る
     * 
     * @param aSourceFile ソースファイル
     * @return 指定�?�れ�?�ソースファイル�?�記述言語�?�対応�?��?� CodeMap オブジェクト
     * @throws NotSupportedSourceFileExeption 指定�?�れ�?�ソースファイル�?��?�対応�?�言語�?�書�?�れ�?��?��?�場�?�
     * @throws IOException 入出力�?�何ら�?��?�例外�?�発生�?��?�場�?�
     */
    public static CodeMap create(final SourceFile aSourceFile) 
    throws NotSupportedSourceFileExeption, IOException
    {
        if ( aSourceFile.isJavaFile() ){
            return new CodeMapForJava(aSourceFile);
        }
        if ( aSourceFile.isCFile() ){
            return new CodeMapForC(aSourceFile);
        }
        
        return null;
    }
}
