package org.computer.aman.metrics.util;

/**
 * ソースコード一行分�?�コードマップを管�?��?�るクラス�?��?�る．
 * <p>
 * コードマップ�?��?�，ソースコード�?�内容�?��?�る�?�文字を<br>
 * (1)コード�?�一部，(2)空白類，(3)コメント文�?�一部，<br>
 * �?��?��?�れ�?��?�分類�?�，�?�ら�?��?��?定�?られ�?�整数値�?��??�?��?列を表�?��?�も�?��?��?�る．
 * <p>
 * (1) �?�定数 CODE，(2) �?�定数 BLANK �?��??れ�?�れ表�?�れる．<br>
 * (3) �?��?��?��?��?�，言語�?�よ�?��?�複数種類�?�コメント文を許�?�場�?�も�?�る�?��?，
 * 本クラスを特化�?��?��?クラス�?�定義�?�れる．
 * 
 * @author Hirohisa AMAN &lt;aman@computer.org&gt;
 */
public class CodeLineMap
{
    /** マップ上�?�，�??�?�文字�?�空白類�?��?�る�?��?�を表�?�値 */
    public static final String BLANK = "0";

    /** マップ上�?�，�??�?�文字�?�コード�?�一部�?��?�る�?��?�を表�?�値 */
    public static final String CODE = "1";
    
    /**
     * 一行分�?�コードマップを設定�?�る． 
     * @param aMap 一行分�?�コードマップ
     */
    public CodeLineMap(final String aMap)
    {
        map = new String(aMap != null ? aMap : "");

        blankCount = 0;
        codeCount = 0;
        commentCount = 0;
        for (int i = 0; i < map.length(); i++ ){
            String ithChar = Character.toString(map.charAt(i));
            if ( BLANK.equals(ithChar) ){
                blankCount++;
            }
            else if ( CODE.equals(ithChar) ){
                codeCount++;
            }
            else{
                commentCount++;
            }
        }
    }
    
    /**
     * 空白類�?�数を返�?�
     * @return 空白類�?�数
     */
    public int getBlankCount()
    {
        return blankCount;
    }

    /**
     * コード文字�?�数を返�?�
     * @return コード文字�?�数
     */
    public int getCodeCount()
    {
        return codeCount;
    }

    /**
     * コメント文字�?�数を返�?�
     * @return コメント文字�?�数
     */
    public int getCommentCount()
    {
        return commentCount;
    }
    
    /**
     * コードマップ（一行分）�?�内容を返�?�
     * @return コードマップ（一行分）�?�内容
     */
    public String getMap()
    {
        return map;
    }
    
    /**
     * 空行（空白類�?��?��?�行，�?��?��?�何も書�?��?��?��?��?�行）�?�ら�?� true，�?�も�?��??�?� false を返�?�
     * 
     * @return 空行�?��?��?��?�
     */
    public boolean isBlankLine()
    {
        return (codeCount == 0) && (commentCount == 0);
    }
    
    /** 空白類�?�数 */
    private int blankCount;
    
    /** コード文字�?�数 */
    private int codeCount;
    
    /** コメント文字�?�数 */
    private int commentCount;
    
    /** マップ:行�?�内容�?�対�?��?��?�文字�?�種類�?�対応�?��?�数字�?�表�?��?�も�?� */
    private String map;
}
