package org.computer.aman.metrics.comment;

/**
 * コメント文�?�測定�?��?果集�?�（C 用）
 * <p>
 * @author Hirohisa AMAN &lt;aman@computer.org&gt;
 */
public class CountResultForC 
extends CountResult
{

    /**
     * // 形�?�?�コメント�?�数を返�?�．
     *
     * @return // 形�?�?�コメント�?�数
     */
    public int getEolCommentCount()
    {
        return eolCommentCount;
    }
    
    /**
     * // 形�?�?�コメント（ファイル�?�先頭�?�記述）�?�数を返�?�．
     *
     * @return // 形�?�?�コメント（ファイル�?�先頭�?�記述）�?�数
     */
    public int getEolCommentCountInHead()
    {
        return eolCommentCountInHead;
    }
    
    /**
     * // 形�?�?�コメントアウト�?�数を返�?�．
     *
     * @return // 形�?�?�コメントアウト�?�数
     */
    public int getEolCommentOutCount()
    {
        return eolCommentOutCount;
    }

    /**
     * C 標準形�?�?�コメント�?�数を返�?�．
     *
     * @return C 標準形�?�?�コメント�?�数
     */
    public int getTraditionalCommentCount()
    {
        return traditionalCommentCount;
    }

    /**
     * C 標準形�?�?�コメント（ファイル�?�先頭�?�記述）�?�数を返�?�．
     *
     * @return C 標準形�?�?�コメント（ファイル�?�先頭�?�記述）�?�数
     */
    public int getTraditionalCommentCountInHead()
    {
        return traditionalCommentCountInHead;
    }

    /**
     * C 標準形�?�?�コメントアウト�?�数を返�?�．
     *
     * @return C 標準形�?�?�コメントアウト�?�数
     */
    public int getTraditionalCommentOutCount()
    {
        return traditionalCommentOutCount;
    }

    /**
     * // 形�?�?�コメント�?�数を +1 �?�る．
     */
    public void incrementEolCommentCount()
    {
        eolCommentCount++;
    }

    /**
     * // 形�?�?�コメント（ファイル�?�先頭�?�記述）�?�数を +1 �?�る．
     */
    public void incrementEolCommentCountInHead()
    {
        eolCommentCountInHead++;
    }

    /**
     * // 形�?�?�コメントアウト�?�数を +1 �?�る．
     */
    public void incrementEolCommentOutCount()
    {
        eolCommentOutCount++;
    }
    
    /**
     * C 標準形�?�?�コメント�?�数を +1 �?�る．
     */
    public void incrementTraditionalCommentCount()
    {
        traditionalCommentCount++;
    }
    
    /**
     * C 標準形�?�?�コメント（ファイル�?�先頭�?�記述）�?�数を +1 �?�る．
     */
    public void incrementTraditionalCommentCountInHead()
    {
        traditionalCommentCountInHead++;
    }
    
    /**
     * C 標準形�?�?�コメントアウト�?�数を +1 �?�る．
     */
    public void incrementTraditionalCommentOutCount()
    {
        traditionalCommentOutCount++;
    }

    /**
     * コメント文�?�測定�?果を文字列�?��?��?��?��?�返�?�．
     * 内容�?�，
     *   EOL コメント，Traditional コメント，Javadoc コメント（未対応�?��?��?0）, （測定部分�?�）直�?�?� EOL コメント，（測定部分�?�）直�?�?� Traditional コメント，
     *   EOL 形�?�?�コメントアウト，Traditional 形�?�?�コメントアウト
     * を�?��?�順番�?�タブ区切り�?�表�?��?�も�?�．
     * 
     * @return コメント文�?�測定�?果
     */
    public String toString()
    {
        return eolCommentCount + "\t" + traditionalCommentCount + "\t" + 0 + "\t" +
               eolCommentCountInHead + "\t" + traditionalCommentCountInHead + "\t" + 
               eolCommentOutCount + "\t" + traditionalCommentOutCount;
    }
    
    /** // 形�?�?�コメント�?�数 */
    private int eolCommentCount;

    /** // 形�?�?�コメント（ファイル�?�先頭�?�記述）�?�数 */
    private int eolCommentCountInHead;

    /** // 形�?�?�コメントアウト�?�数 */
    private int eolCommentOutCount;

    /** C 標準形�?�?�コメント�?�数 */
    private int traditionalCommentCount;

    /** C 標準形�?�?�コメント（ファイル�?�先頭�?�記述）�?�数 */
    private int traditionalCommentCountInHead;

    /** C 標準形�?�?�コメントアウト�?�数 */
    private int traditionalCommentOutCount;
}
