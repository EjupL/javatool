package org.computer.aman.metrics.comment;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

import org.computer.aman.io.sourcecode.NotSupportedSourceFileExeption;

public class CommentCounterCUI
{
    public static void main(String[] args) 
    throws SecurityException, NotSupportedSourceFileExeption, IOException
    {
        CommentCounterCUI ui = new CommentCounterCUI();

        ui.printCopyright();
        if ( args.length > 0 ){
            ui.printUsage();
            return;
        }
        printSeparator();
        
        // 標準入力�?�ら，「ファイルパス，開始行，終了行�?�?�三�?�組�?�（�?��?��?�，タブ区切り）を繰り返�?�読�?�出�?�
        // �??�?��?�，当該ファイル�?�対応�?��?� CommentCounter オブジェクトを用�?�?�る．
        // �?��?��?�，通常�?��?��?�ファイルパス�?��?��??�?��?�連続�?�る�?��?�，ファイルパス�?��?�?�行�?�異�?��?��?�時�?��?��?�
        // オブジェクトを生�?�?�る．
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String line = null;
        CommentCounter counter = null;
        while ( (line = reader.readLine()) != null ){
            Scanner scanner = new Scanner(line).useDelimiter("\t");
            String path = scanner.next();
            int begin = scanner.nextInt();
            int end = scanner.nextInt();

            if ( counter == null || !path.equals(counter.getFilePath()) ){
                counter = CommentCounterFactory.create(path);
            }

            System.out.println(path + "\t" + begin + "\t" + end + "\t" + counter.measure(begin, end));
        }
        printSeparator();
    }
    
    /**
     * 区切り線を標準エラー出力�?�表示�?�る．
     */
    private static void printSeparator()
    {
        final int LENGTH = 64;
        for ( int i = 0; i < LENGTH; i++ ){
            System.err.print("-");
        }
        System.err.println();
    }
    
    /**
     * �?ージョン情報�?� Copyright を標準エラー出力�?�表示�?�る．
     */
    private void printCopyright()
    {
        try {
            printResource(COPYRIGHT);
        }
        catch (IOException e) {
            System.err.println("�?ージョン情報ファイル�?�読�?�出�?��?�エラー�?�起�?�り�?��?��?�（無視�?��?�実行�?��?��?�）");
        }
    }
    
    /**
     * 指定�?�れ�?�リソース（テキストファイル）�?�内容を標準エラー出力�?�表示�?�る．
     * 
     * @param aFileName 対象�?��?�るテキストファイル
     * @throws IOException リソース�?�読�?�出�?��?�失敗�?��?�場�?�
     */
    private void printResource(final String aFileName) 
    throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(getClass().getClassLoader().getResourceAsStream(aFileName)));
        String line = null;
        while ( (line = reader.readLine()) != null){
            System.err.println(line);
        }
        reader.close();
    }
    
    /**
     * 使�?�方を標準エラー出力�?�出力�?�る．
     */
    private void printUsage()
    {
        try {
            printResource(USAGE);
        }
        catch (IOException e) {
            System.err.println("使用方法�?�説明ファイル�?�読�?�出�?��?�エラー�?�起�?�り�?��?��?�");
        }    
    }
    
    private final String COPYRIGHT = "copyright.txt";
    private final String USAGE = "usage.txt";
}
