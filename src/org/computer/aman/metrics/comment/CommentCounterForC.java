package org.computer.aman.metrics.comment;

import java.io.IOException;
import java.util.Iterator;

import org.computer.aman.io.sourcecode.NotSupportedSourceFileExeption;
import org.computer.aman.io.sourcecode.SourceFile;
import org.computer.aman.metrics.util.CodeLineMap;
import org.computer.aman.metrics.util.c.CodeLineMapForC;

/**
 * C ソースファイル中�?�メソッド�?��?��?��?�コメント文�?�測定�?��??れ�?�関連�?�る機能を�??供
 * <p>
 * @author Hirohisa AMAN &lt;aman@computer.org&gt;
 */
public class CommentCounterForC 
extends CommentCounter
{

    /**
     * 指定�?�れ�?�ソースファイルを測定対象�?��?��?� CommentCounterForC オブジェクトを生�?�?�る
     * 
     * @param aSourceFile 測定対象�?�ソースファイル
     */
    public CommentCounterForC(final SourceFile aSourceFile) 
    throws NotSupportedSourceFileExeption, IOException
    {
        super(aSourceFile);
    }
    
    /**
     * コメント文�?�測定を実行�?�，�?果を CommentCountResultSet インスタンス�?��?��?�返�?�．
     * 
     * @param aBeginLineNumber 測定�?�開始行
     * @param anEndLineNumber 測定�?�終了行
     * @return コメント文測定�?��?果
     */
    public CountResult measure(final int aBeginLineNumber, final int anEndLineNumber) 
    throws IOException, NotSupportedSourceFileExeption 
    {
        CountResultForC results = new CountResultForC();

        // (1) 測定対象メソッド�?�直�?�?�書�?�れ�?�コメントを測定�?�る
        // (1-1) 測定対象行よりも�?�?�登場�?�る実効コード行�?�中�?�一番後(predecessorLine)を見�?��?�る
        // (1-2) predecessorLine+1 �?�ら aBeginLineNumber-1 �?��?��?�行�?�登場�?�るコメントをカウント�?�る        
        Iterator<CodeLineMap> itr = getCodeMap().iterator();
        
        int predecessorLine = -1;
        for ( int i = 1; i < aBeginLineNumber; i++ ){
            if ( itr.next().getCodeCount() > 0 ){
                predecessorLine = i;
            }
        }
        
        if ( predecessorLine != -1 ){
            itr = getCodeMap().iterator();
            for ( int i = 1; i < aBeginLineNumber; i++ ){
                CodeLineMapForC lineMap = (CodeLineMapForC)itr.next();
                if ( i <= predecessorLine || lineMap.getCommentCount() == 0 ){
                    continue;
                }
                else if ( lineMap.getEolCommentCount() > 0 ){
                    results.incrementEolCommentCountInHead();
                }
                else{
                    results.incrementTraditionalCommentCountInHead();
                }
            }
        }
        
        // (2) (1) �?�続�??�?�ら一行�?��?�マップを見�?��?��??
        for ( int i = aBeginLineNumber; i <= anEndLineNumber; i++ ){
            CodeLineMapForC lineMap = (CodeLineMapForC)itr.next();                        
            if ( lineMap.getCommentCount() == 0 ){
                continue;
            }
            results.incrementCommentCount();
            // コメント�?�内訳�?�カウント
            // 異�?�るタイプ�?�コメント文�?�一行�?�混在�?�る場�?��?� EOLコメントアウト，Traditional コメントアウト，EOL, Traditional �?�優先順�?�カウント�?�る 
            if ( lineMap.getTraditionalCommentOutCount() > 0 ){
                results.incrementTraditionalCommentOutCount();
            }
            else if ( lineMap.getEolCommentOutCount() > 0 ){
                results.incrementEolCommentOutCount();
            }
            else if ( lineMap.getEolCommentCount() > 0 ){
                results.incrementEolCommentCount();
            }
            else{
                results.incrementTraditionalCommentCount();
            }
        }
        
        return results;
    }
}
