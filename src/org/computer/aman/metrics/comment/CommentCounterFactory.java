package org.computer.aman.metrics.comment;

import java.io.IOException;

import org.computer.aman.io.sourcecode.NotSupportedSourceFileExeption;
import org.computer.aman.io.sourcecode.SourceFile;

/**
 * 指定�?�れ�?�ソースファイルを測定対象�?��?��?� CommentCounter オブジェクトを生�?�?�るファクトリ
 * 
 * @author Hirohisa AMAN &lt;aman@computer.org&gt;
 */
public class CommentCounterFactory
{
    /**
     * 指定�?�れ�?�ソースファイルを測定対象�?��?��?� CommentCounter オブジェクトを生�?�?�る
     * 
     * @param aFilePath 測定対象�?�ソースファイルパス
     */
    public static CommentCounter create(final String aFilePath) 
    throws SecurityException, NotSupportedSourceFileExeption, IOException
    {
        SourceFile file = new SourceFile(aFilePath);
        
        if ( file.isJavaFile() ){
            return new CommentCounterForJava(file);
        }
        if ( file.isCFile() ){
            return new CommentCounterForC(file);
        }
        
        throw new NotSupportedSourceFileExeption("not supported file type: " + aFilePath);    
    }
    
}
