package org.computer.aman.metrics.comment;

/**
 * コメント文�?�測定�?��?果集�?�
 * <p>
 * @author Hirohisa AMAN &lt;aman@computer.org&gt;
 */
public class CountResult
{
    /**
     * コメント文�?�登場�?�る行数を返�?�．
     *
     * @return コメント文�?�登場�?�る行数
     */
    public int getCommentCount()
    {
        return commentCount;
    }
    
    /**
     * コメント文�?�登場�?�る行数を +1 �?�る．
     */
    public void incrementCommentCount()
    {
        commentCount++;
    }

    /** コメント文�?�登場�?�る行数 */
    private int commentCount;
}
