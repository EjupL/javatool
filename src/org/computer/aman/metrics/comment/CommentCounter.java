package org.computer.aman.metrics.comment;

import java.io.IOException;

import org.computer.aman.io.sourcecode.NotSupportedSourceFileExeption;
import org.computer.aman.io.sourcecode.SourceFile;
import org.computer.aman.metrics.util.CodeMap;
import org.computer.aman.metrics.util.CodeMapFactory;

/**
 * メソッド�?��?�?��?�コメント計上を行�?�測定器
 * 
 * @author Hirohisa AMAN &lt;aman@computer.org&gt;
 */
public abstract class CommentCounter
{
    /**
     * 指定�?�れ�?�ソースファイルを測定対象�?��?��?� MethodCommentCounter オブジェクトを生�?�?�る
     * （�?��?�クラス�?�抽象クラス�?��?�る�?��?，�?��?�コンストラクタ�?�サブクラス�?�コンストラクタ�?�ら�?��?�呼�?�出�?�れる）
     * 
     * @param aSourceFile 測定対象�?�ソースファイル
     * @throws IOException 
     * @throws NotSupportedSourceFileExeption 
     */
    public CommentCounter (final SourceFile aSourceFile) 
    throws NotSupportedSourceFileExeption, IOException
    {
        sourceFile = aSourceFile;
        codeMap = CodeMapFactory.create(aSourceFile);
    }
    
    /**
     * 測定対象ソースファイル�?�パスを返�?�
     * 
     * @return 測定対象ソースファイル�?�パス
     */
    public String getFilePath()
    {
        return sourceFile.getPath();
    }
    
    /**
     * コメント文�?�測定を実行�?�，�?果を CommentCountResultSet インスタンス�?��?��?�返�?�．
     * 
     * @param aBeginLineNumber 測定�?�開始行
     * @param anEndLineNumber 測定�?�終了行
     * @return コメント文測定�?��?果
     */
    public abstract CountResult measure(final int aBeginLineNumber, final int anEndLineNumber)
    throws IOException, NotSupportedSourceFileExeption ;
    
    
    /**
     * 対象�?��?��?��?�るソースコード�?�コードマップを返�?�（サブクラス�?��?�）
     * 
     * @return 対象�?��?��?��?�るソースコード�?�コードマップ
     */
    protected CodeMap getCodeMap()
    {
        return codeMap;
    }
    
    /** 測定対象ソースファイル�?�コードマップ */
    private CodeMap codeMap;
    
    /** 測定対象�?�ソースファイル */
    private SourceFile sourceFile;

}
