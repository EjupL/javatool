package main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.FileUtils;

import uk.ac.open.crc.intt.IdentifierNameTokeniser;
import uk.ac.open.crc.intt.IdentifierNameTokeniserFactory;

public class TokenisationExample {
	public static void main(String[] args) throws Exception {

		IdentifierNameTokeniserFactory factory = new IdentifierNameTokeniserFactory();
		IdentifierNameTokeniser tokeniser = factory.create();

		List<String[]> listTokens = new ArrayList<String[]>();
		int counter = 0;

		try {
			BufferedReader inputFile = new BufferedReader(new FileReader(
					new File("D:\\Hibernate\\HibernateMethods.txt")));

			String line;
			String[] tokens;
			int currentLine = 0;
			while (true) {
				

				line = inputFile.readLine();
				if (line == null) {
					break;
				}

				currentLine++;

				if (currentLine >= 0) {
					tokens = tokeniser.tokenise(line);

					listTokens.add(tokens);
					for (int i = 0; i < tokens.length; i++) {
						System.out.print(tokens[i]);

						if (i < tokens.length - 1) {
							System.out.print(" ");
						}

					}

					System.out.println();
					counter++;
				}
			}

			inputFile.close();
		} catch (IOException ioEx) {
			System.err.println(ioEx.getMessage());
			System.exit(2);
		}
	}
}
