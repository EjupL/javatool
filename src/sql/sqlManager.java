package sql;

import interfaces.InterfaceSQLCredentials;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;

import org.json.JSONException;

import proxyManager.ExtractProxys;
import cmd.GitCmd;

public class sqlManager extends SqlCredintials {

	public void insertMethods(String projectName, String aMethodName,
			String aMethodPath, int aMethodLineStart, int aMethodLineEnd,
			String aMethodType, int InnerCommentT1, int InnerCommentT2,
			int InnerCommentT3, int BeforeMethodT1, int BeforeMethodT2,
			int CommentetOutT1, int CommentetOutT2, String commitID) // arguments
	{

		Connection connection = null;

		try {
			connection = DriverManager.getConnection(url, username, password);
			// System.out.println("Inserting Query...");
			String sql = "INSERT INTO methodsdetails ("
					+ "ProjectName, MethodName, MethodPath, MethodLineStart, MethodLineEnd, MethodType,"
					+ "InnerCommentT1, InnerCommentT2, InnerCommentT3, "
					+ "BeforeMethodT1, BeforeMethodT2, "
					+ "CommentetOutT1, CommentetOutT2, commitID)"
					+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

			PreparedStatement preparedStatement = connection
					.prepareStatement(sql);

			preparedStatement.setString(1, projectName);// varchar
			preparedStatement.setString(2, aMethodName);// varchar
			preparedStatement.setString(3, aMethodPath);// varchar
			preparedStatement.setInt(4, aMethodLineStart);// int
			preparedStatement.setInt(5, aMethodLineEnd);// int
			preparedStatement.setString(6, aMethodType);// varchar
			preparedStatement.setInt(7, InnerCommentT1);// int
			preparedStatement.setInt(8, InnerCommentT2);// int
			preparedStatement.setInt(9, InnerCommentT3);// int
			preparedStatement.setInt(10, BeforeMethodT1);// int
			preparedStatement.setInt(11, BeforeMethodT2);// int
			preparedStatement.setInt(12, CommentetOutT1);// int
			preparedStatement.setInt(13, CommentetOutT2);// int
			preparedStatement.setString(14, commitID);// str
			preparedStatement.executeUpdate();

			// System.out.println("Inserted Succesfully.....");

		} catch (SQLException e) {
			throw new RuntimeException("Cannot connect the database!", e);
		} finally {
			// System.out.println("Closing the connection.");
			if (connection != null)
				try {
					connection.close();
				} catch (SQLException ignore) {
				}
		}
	}

	public static void rundDriver() {
		try {
			// The newInstance() call is a work around for some
			// broken Java implementations

			System.out.println("Loading driver...");
			Class.forName("com.mysql.jdbc.Driver");
			System.out.println("Driver loaded!");
		} catch (Exception ex) {
			// handle the error
		}
	}

	@SuppressWarnings("unused")
	public static void _insertCommitId(String aPath)
			throws NumberFormatException, IOException, SQLException {

		Connection connection = DriverManager.getConnection(url, username,
				password);

		String query = "SELECT ID, MethodName, MethodLineStart, MethodLineEnd, MethodPath FROM methodsdetails";
		// create the java statement
		Statement st = connection.createStatement();

		// execute the query, and get a java resultset
		ResultSet rs = st.executeQuery(query);

		// iterate through the java resultset
		while (rs.next()) {
			int id = rs.getInt("ID");
			String methodNme = rs.getString("MethodName");
			String startLine = rs.getString("MethodLineStart");
			String endLine = rs.getString("MethodLineEnd");
			String methodPath = rs.getString("MethodPath");

			// print the results
			String commitId = GitCmd.gitLog(aPath, methodPath,
					Integer.parseInt(startLine), Integer.parseInt(endLine));
			System.out.println(commitId);
			/*
			 * String insertQuery =
			 * "INSERT INTO commitId (CommitId, apiUrl) VALUES (?, ?)";
			 * 
			 * PreparedStatement preparedStatement =
			 * connection.prepareStatement(insertQuery);
			 * preparedStatement.setString(1, commitId);//varchar
			 * preparedStatement.setString(2,
			 * "https://api.github.com/repos/hibernate/hibernate-orm/commits/");
			 * try { preparedStatement.executeUpdate(); } catch (SQLException e)
			 * { continue; }
			 */

			/*
			 * String insertQuery1 =
			 * "UPDATE  methodsdetails SET CommitId = ?  WHERE ID = " + id +
			 * " ";
			 * 
			 * PreparedStatement preparedStatement1 =
			 * connection.prepareStatement(insertQuery1);
			 * preparedStatement1.setString(1, commitId);//varchar
			 * preparedStatement1.executeUpdate();
			 */
		}
		st.close();
	}

	public static void selectCommitId_InsertProxys() throws JSONException,
			Exception {

		Connection connection = DriverManager.getConnection(url, username,
				password);

		String query = "SELECT * FROM `commitid`";

		Statement st = connection.createStatement();

		// execute the query, and get a java resultset
		ResultSet rs = st.executeQuery(query);

		while (rs.next()) {

			String commitId = rs.getString("CommitId");
			String projectURL = rs.getString("projectURL");

			ExtractProxys proxy = new ExtractProxys();

			Map<String, Integer> results = proxy.extraxtProxys(projectURL,
					commitId);

			System.out.println(results.get("longestPrefix"));
			System.out.println(results.get("additions"));
			System.out.println(results.get("extensions"));
			System.out.println(results.get("numberOfFiles"));

			insertProxys(results.get("additions"), results.get("extensions"),
					results.get("longestPrefix"), results.get("numberOfFiles"),
					commitId);

		}

		connection.close();

	}

	public static void insertProxys(int additions, int extensions,
			int longestPrefix, int numberOfFiles, String commitId)
			throws SQLException {

		Connection connection = DriverManager.getConnection(url, username,
				password);
		String insertQuery = "INSERT INTO proxys (longestPrefix, Additions, Extensions, NumberOfFiles, commitId) VALUES (?, ?, ?, ?, ?)";

		PreparedStatement preparedStatement = connection
				.prepareStatement(insertQuery);
		preparedStatement.setInt(1, longestPrefix);
		preparedStatement.setInt(2, additions);
		preparedStatement.setInt(3, extensions);
		preparedStatement.setInt(4, numberOfFiles);
		preparedStatement.setString(5, commitId);

		preparedStatement.executeUpdate();
		connection.close();
	}

}
