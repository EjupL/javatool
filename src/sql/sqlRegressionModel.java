package sql;

import interfaces.InterfaceSQLCredentials;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import regression_model.RegressionModels;

public class sqlRegressionModel extends SqlCredintials {

	public static Map<String, Integer> getProxys(String proxy)
			throws SQLException {

		Connection connection = DriverManager.getConnection(url, username,
				password);

		String query = "SELECT " + proxy + ", commitId FROM `proxys`";

		Statement st = connection.createStatement();

		// execute the query, and get a java resultset
		ResultSet rs = st.executeQuery(query);

		Map<String, Integer> additionsMap = new HashMap<String, Integer>();

		while (rs.next()) {
			String commitId = rs.getString("commitId");
			int aProxy = rs.getInt(proxy);

			additionsMap.put(commitId, aProxy);

		}
		connection.close();
		return additionsMap;
	}

	public static void calculateTimeEffort() throws SQLException {
		String url = "jdbc:mysql://localhost:3306/methods";
		String username = "root";
		String password = "";
		Connection connection = DriverManager.getConnection(url, username,
				password);

		String query = "SELECT p.* FROM methods.proxys p, methods.commitid c WHERE c.CommitId = p.commitId";

		Statement st = connection.createStatement();

		// execute the query, and get a java resultset
		ResultSet rs = st.executeQuery(query);
		while (rs.next()) {
			String commitId = rs.getString("commitId");
			int longestPrefix = rs.getInt("longestPrefix");
			int additions = rs.getInt("Additions");
			int extensions = rs.getInt("Extensions");
			int numberOfFiles = rs.getInt("NumberOfFiles");

			String insertQuery = "INSERT INTO effort (commitId, LongestCommonPrefix, Additions, Extensions, NumberOfFiles)"
					+ " VALUES (?, ?, ?, ?, ?) ";

			// prepare the regression model
			RegressionModels number_of_files = new RegressionModels(316.04,
					3.428);
			RegressionModels longest_common_prefix = new RegressionModels(
					443.252, -2.387);
			RegressionModels additions_deletions = new RegressionModels(276,
					0.22439);
			RegressionModels number_of_extensions = new RegressionModels(
					197.57, 100.05);

			// insert values to database
			PreparedStatement preparedStatement = connection
					.prepareStatement(insertQuery);
			preparedStatement.setString(1, commitId);
			preparedStatement.setDouble(2,
					longest_common_prefix.calculateLinearModel(longestPrefix));
			preparedStatement.setDouble(3,
					additions_deletions.calculateLinearModel(additions));
			preparedStatement.setDouble(4,
					number_of_extensions.calculateLinearModel(extensions));
			preparedStatement.setDouble(5,
					number_of_files.calculateLinearModel(numberOfFiles));

			preparedStatement.executeUpdate();

		}
		connection.close();
	}
}
