package sql;

import interfaces.InterfaceSQLCredentials;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class GetChangedFiles extends SqlCredintials {

	private List<String> files = new ArrayList<String>();

	public List<String> getAllFiles(String projectName, int projectVersion)
			throws SQLException {

		Connection connection = DriverManager.getConnection(url, username,
				password);

		String query = "SELECT DISTINCT MethodPath FROM methodsdetails INNER JOIN commitid ON commitid.ProjectVersion = "
				+ projectVersion
				+ " AND commitid.ProjectName = \""
				+ projectName + "\"";
		// create the java statement
		Statement st = connection.createStatement();

		// execute the query, and get a java resultset
		ResultSet rs = st.executeQuery(query);
		while (rs.next()) {
			String methodPath = rs.getString("MethodPath");

			files.add(methodPath);
		}
		connection.close();
		return files;
	}

}
