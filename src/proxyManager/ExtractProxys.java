package proxyManager;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.sql.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import urlParsers.JsonReader;

public class ExtractProxys {

	private ArrayList<String> filesChanged = new ArrayList<String>();
	private Map<InputStream, String> filesStream = new HashMap<InputStream, String>();

	public ArrayList<String> getFilesChanged() {
		return filesChanged;
	}

	public Map<InputStream, String> getFilesStream() {
		return filesStream;
	}

	private int longestCommonPrefix(Object[] objects) {
		if (objects.length == 0) {
			return 0; // Or maybe return null?
		}

		for (int prefixLen = 0; prefixLen < ((String) objects[0]).length(); prefixLen++) {
			char c = ((String) objects[0]).charAt(prefixLen);
			for (int i = 1; i < objects.length; i++) {
				if (prefixLen >= ((String) objects[i]).length()
						|| ((String) objects[i]).charAt(prefixLen) != c) {
					// Mismatch found
					return ((String) objects[i]).substring(0, prefixLen)
							.length();
				}
			}
		}
		return objects[0].toString().length();
	}

	// args: url=api of the project on github; commitID= id of the commit
	public Map<String, Integer> extraxtProxys(String url, String commitID)
			throws JSONException, Exception {
		JSONArray files = null;
		JSONObject stats = null;
		// remoce white spaces from url
		String cURL = url.replaceAll("\\s+", "");
		// read the json url and get the results
		JSONObject json = new JSONObject(JsonReader.readUrl(cURL + commitID));
		if (json == null)
			return null;

		Map<String, Integer> proxys = new HashMap<String, Integer>();
		Map<String, URL> fileChangedMap = new HashMap<String, URL>();
		// get the json details
		stats = json.getJSONObject("stats");

		// get files that changed on this commit
		files = json.getJSONArray("files");

		Map<String, Integer> extension = new HashMap<String, Integer>();

		// iterate through all files and extract file extensions, filenames,
		// number of files that changed
		for (int i = 0; i < files.length(); i++) {
			JSONObject obj = files.getJSONObject(i);
			String fileName = obj.getString("filename").toString();

			filesChanged.add(fileName);
			URL rawURL = new URL(obj.getString("raw_url").toString());
			fileChangedMap.put(fileName, rawURL);

			String ext = FilenameUtils.getExtension(obj.getString("filename"));

			// increment number of extensions if exist, otherwise create key=ext
			// value=number
			if (extension.containsKey(ext))
				extension.put(ext, extension.get(ext) + 1);
			else {
				extension.put(ext, 1);
			}

		}

		for (Map.Entry<String, URL> entry : fileChangedMap.entrySet()) {
			File f = new File("D:\\SpringTest\\" + entry.getKey());
			// if file is java, save to local drive so later we can access for
			// extracting methods, comments, etc.
			if (FilenameUtils.getExtension(entry.getKey()).equals("java")
					&& filesChanged.size() <= 100)
				FileUtils.copyURLToFile(entry.getValue(), f);
		}

		// get longest common prefix for files that changed on this commit
		int longesPrefix;
		longesPrefix = longestCommonPrefix(filesChanged.toArray());

		// put desired results on Map with key and value
		proxys.put("additions", stats.getInt("total"));// return statistics for
														// all additions and
														// deletions
		proxys.put("extensions", extension.size());// return the size of
													// extension array. This
													// will indicate how many
													// different exten. are in
													// the array
		proxys.put("longestPrefix", longesPrefix); // return longest common
													// prefix
		proxys.put("numberOfFiles", filesChanged.size()); // returns the total
															// number of files
															// that have changed
															// on the commit

		return proxys;

	}
}
