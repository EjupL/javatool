package cmd;

public class test {

	public String splitString(String argument) {
		/*
		 * Split the string based on white spaces and save the results in
		 * newString array.
		 */
		String[] newString = argument.split("\\s+");
		// return the first element from the newString array
		return newString[0];
	}
}
