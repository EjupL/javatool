package namingConvention;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
//import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;

import sql.SqlCredintials;

public class CheckMethodsNames extends SqlCredintials {

	// to do:
	// for each method check if the methods is:
	// in dictionary, number of words, is capitalized each method name, and save
	// to database
	public void analyzeMethodsForProject(String projectName)
			throws JSONException, Exception {
		Connection connection = DriverManager.getConnection(url, username,
				password);
		// Map<Integer, HashMap> methods = new HashMap<Integer, HashMap>();
		String query = "SELECT ID, ProjectName, MethodName, MethodLineStart, MethodLineEnd, Changed, MethodPath, CommitId FROM "
				+ " methodsdetails WHERE Changed=1 AND ProjectName = '"
				+ projectName + "' ";
		// create the java statement
		Statement st = connection.createStatement();
		ResultSet rs = st.executeQuery(query);

		while (rs.next()) {
			int id = rs.getInt("ID");
			String methodName = rs.getString("MethodName");
			// int startLine =
			// Integer.parseInt(rs.getString("MethodLineStart"));
			// int endLine = Integer.parseInt(rs.getString("MethodLineEnd"));
			// String methodPath = rs.getString("MethodPath");
			// String commitId = rs.getString("CommitId");

			Tokenisation tokenise = new Tokenisation();

			String[] tokenisedMethod = tokenise.tokeniseMethod(methodName);
			// check if the words are in dictionary
			Boolean dictionaryResult = dictionaryCheck(tokenisedMethod);
			// Check if words are capitalized properly
			Boolean capitalizationAnomaly = checkCapitalizaonAnomaly(tokenisedMethod);
			// check number of words for method
			int NumberOfWords = tokenisedMethod.length + 1;

			insertResultsToDatabase(projectName, dictionaryResult,
					capitalizationAnomaly, NumberOfWords, id, connection);

		}
		connection.close();
	}

	// *Check if the words of method are part of the english dictionary
	private Boolean dictionaryCheck(String[] tokenisedMethod) {
		DictionaryChecker dictionary = new DictionaryChecker();
		Map<Integer, String> englishDictionary = dictionary
				.createDictionary("D:\\workspace\\InnerComments\\EnglishDictionary.txt");
		Map<Integer, String> acronymsDictionary = dictionary
				.createDictionary("D:\\workspace\\InnerComments\\Acronyms.txt");

		List<Boolean> results = new ArrayList<Boolean>();

		for (int i = 0; i < tokenisedMethod.length; i++) {

			String tokenisedMethodName = tokenisedMethod[i].toLowerCase()
					.substring(tokenisedMethod[i].lastIndexOf(" ") + 1);

			if (englishDictionary.containsValue(tokenisedMethodName)) {
				results.add(true);
			} else {
				if (acronymsDictionary.containsValue(tokenisedMethodName))
					results.add(true);
				else
					results.add(false);
			}

		}
		System.out.println(Arrays.toString(tokenisedMethod));

		if (results.contains(false))
			return false;
		else
			return true;

	}

	// *Check if words are capitalized properly e.g. thisMethodFunction is
	// properly capitalized
	private Boolean checkCapitalizaonAnomaly(String[] tokenisedMethod) {
		List<Boolean> results = new ArrayList<Boolean>();
		DictionaryChecker dictionary = new DictionaryChecker();
		Map<Integer, String> acronymsDictionary = dictionary
				.createDictionary("D:\\workspace\\InnerComments\\Acronyms.txt");

		for (int i = 1; i < tokenisedMethod.length; i++) {
			//this is only to get actronyms to lower case
			String tokenisedMethodName = tokenisedMethod[i].toLowerCase()
					.substring(tokenisedMethod[i].lastIndexOf(" ") + 1);

			if (tokenisedMethod[i].contains("_"))
				results.add(false);

			if (Character.isUpperCase(tokenisedMethod[i].charAt(0)))
				results.add(true);

			// Here checks if characters are lowercase except 1st one
			// but first checks if method name is an acronym e.g. XML
			if (!acronymsDictionary.containsValue(tokenisedMethodName)) {
				for (int j = 1; j < tokenisedMethod[i].length(); j++) {

					if (Character.isUpperCase(tokenisedMethod[i].charAt(j)))
						results.add(false);
				}
			}
		}

		if (results.contains(false))
			return false;
		else
			return true;
	}

	private void insertResultsToDatabase(String projectName,
			Boolean dictionaryResult, Boolean capitalizationAnomaly,
			int NumberOfWords, int methodId, Connection connection)
			throws SQLException {
		String insertQuery = "INSERT INTO method_naming_convention "
				+ "(ProjectName, Dictionary, CapitalizationAnomaly, NumberOfWords, MethodId) "
				+ "VALUES (?, ?, ?, ?, ? )";

		PreparedStatement ps = connection.prepareStatement(insertQuery);

		ps.setString(1, projectName);
		ps.setBoolean(2, dictionaryResult);
		ps.setBoolean(3, capitalizationAnomaly);
		ps.setInt(4, NumberOfWords);
		ps.setInt(5, methodId);

		ps.executeUpdate();

	}
}
