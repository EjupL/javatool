package namingConvention;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import uk.ac.open.crc.intt.*;

public class Tokenisation {

	public String[] tokeniseMethod(String methodName) {
		IdentifierNameTokeniserFactory factory = new IdentifierNameTokeniserFactory();
		IdentifierNameTokeniser tokeniser = factory.create();
		String[] tokenisedMethodName = tokeniser.tokenise(methodName
				.split("\\(")[0]);
		return tokenisedMethodName;

	}

	public String[] tokeniseMethod(int startLine, int endLine, InputStream file) {

		IdentifierNameTokeniserFactory factory = new IdentifierNameTokeniserFactory();
		IdentifierNameTokeniser tokeniser = factory.create();

		List<String[]> listTokens = new ArrayList<String[]>();
		// int counter = 0;
		String[] method = null;
		try {
			BufferedReader inputFile = new BufferedReader(
					new InputStreamReader(file));

			String line;
			String[] tokens;
			int currentLine = 0;
			while (true) {
				if (currentLine == endLine)
					break;

				line = inputFile.readLine();
				if (line == null) {
					break;
				}

				currentLine++;

				if (currentLine >= startLine) {

					tokens = tokeniser.tokenise(line);

					if (!line.startsWith("@")) {
						if (currentLine == startLine)
							method = tokeniser.tokenise(line.split("\\(")[0]);
					}
					// listTokens.add(tokens);
					// for (int i = 0; i < tokens.length; i++) {
					// System.out.print(tokens[i]);
					//
					// if (i < tokens.length - 1) {
					// System.out.print(" ");
					// }
					//
					// }

				}
			}

			inputFile.close();
			return method;
		} catch (IOException ioEx) {
			System.err.println(ioEx.getMessage());
			System.exit(2);
		}
		return method;
	}
}
