package lawOfDemeter;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

import javax.print.DocFlavor.STRING;

import org.eclipse.jgit.ignore.internal.Strings;
import org.eclipse.jgit.util.StringUtils;

public class CheckLawOfDemeter {

	public Tuple analyzeLawOfDemeter(int startLine, int endLine,
			InputStream file) {
		int dots = 0;
		int semiColons = 0;
		try {

			BufferedReader reader = new BufferedReader(new InputStreamReader(
					file));
			String line;
			int currentLine = 0;
			while (true) {
				if (currentLine == endLine)
					break;

				line = reader.readLine();

				if (line == null)
					break;

				currentLine++;

				if (currentLine >= startLine) {
					if (line.contains("/"))
						continue;

					String getDots = line.replaceAll("[^.]", "");
					int dotsLength = getDots.length();
					dots += dotsLength;

					String getSemiColons = line.replaceAll("[^;]", "");
					int semiColonLength = getSemiColons.length();
					semiColons += semiColonLength;

				}

			}
			reader.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return new Tuple(dots, semiColons);
	}

}
