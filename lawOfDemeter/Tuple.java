package lawOfDemeter;

public class Tuple {

	int dots, semicolons;

	Tuple(int a, int b) {
		dots = a;
		semicolons = b;
	}

	public int getDots() {
		return this.dots;
	}

	public int getSemicolons() {
		return this.semicolons;
	}
}
