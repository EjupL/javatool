package extractor;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.computer.aman.io.sourcecode.NotSupportedSourceFileExeption;
import org.computer.aman.metrics.comment.CommentCounter;
import org.computer.aman.metrics.comment.CommentCounterFactory;
import org.computer.aman.metrics.util.method_extract.JavaMethodExtractor;
import org.computer.aman.metrics.util.method_extract.ParseException;

import cmd.RunCmd;
import sql.sqlManager;
import functionHelpers.splitResults;

public class MethodAndCommentExtractor {

	public static Map<String, Integer> extractComments(String filePath,
			int startLine, int endLine) throws SecurityException,
			NotSupportedSourceFileExeption, IOException {
		CommentCounter counter = null;
		Map<String, Integer> comments = new HashMap<String, Integer>();

		if (counter == null)
			counter = CommentCounterFactory.create(filePath);

		comments = splitResults.splitCommentCounter(counter.measure(startLine,
				endLine).toString());
		// System.out.println(comments);
		return comments;

	}

	public static void extractMethodsAndComments(String filePath,
			CommentCounter counter) throws Exception {

		Map<String, String> values = new HashMap<String, String>();
		Map<String, Integer> comments = new HashMap<String, Integer>();
		// CommentCounter counter = null;
		JavaMethodExtractor extractedMethod = new JavaMethodExtractor(
				new FileInputStream(filePath));
		@SuppressWarnings("unused")
		sqlManager sql = new sqlManager();

		try {
			extractedMethod.CompilationUnit();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		for (Iterator<String> itr = extractedMethod.getMemberList().iterator(); itr
				.hasNext();) {

			String line = filePath + "\t" + itr.next();
			// split line will return value with key: path, method, methodName,
			// methodType, startLine and endLine
			values = splitResults.splitMethodExtractor(line);

			if (counter == null) {
				counter = CommentCounterFactory.create(values.get("path"));

			}

			comments = splitResults.splitCommentCounter(counter.measure(
					Integer.parseInt(values.get("startLine")),
					Integer.parseInt(values.get("endLine"))).toString());

			// sql.insertMethods(values.get("methodName"), values.get("path"),
			// Integer.parseInt(values.get("startLine")),
			// Integer.parseInt(values.get("endLine")),
			// values.get("methodType"), comments.get("1"), comments.get("2"),
			// comments.get("3"), comments.get("4"), comments.get("5"),
			// comments.get("6"), comments.get("7"));

			System.out.println(comments.get("1") + "\t" + comments.get("2")
					+ "\t" + values.get("path") + "\t"
					+ values.get("startLine") + "\t" + values.get("endLine")
					+ "\t" + values.get("methodName"));

			// RunCmd.runCmdCommand("D:\\Hibernate\\", filePath);

		}

	}

	public static void extractMethods(String filePath)
			throws SecurityException, NotSupportedSourceFileExeption,
			IOException, ParseException {

		Map<String, String> values = new HashMap<String, String>();

		JavaMethodExtractor extractedMethod;

		extractedMethod = new JavaMethodExtractor(new FileInputStream(filePath));

		// extractedMethod.CompilationUnit();
		try {
			extractedMethod.CompilationUnit();

			for (Iterator<String> itr = extractedMethod.getMemberList()
					.iterator(); itr.hasNext();) {

				String line = filePath + "\t" + itr.next();
				// split line will return value with key: path, method,
				// methodName, methodType, startLine and endLine
				values = splitResults.splitMethodExtractor(line);
				System.out.println(values.get("methodName") + "\t"
						+ values.get("startLine") + "\t"
						+ values.get("endLine"));
			}
			extractedMethod.getMemberList().clear();

		} catch (Exception e) {

		}

	}

}
