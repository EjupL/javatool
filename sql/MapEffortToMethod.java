package sql;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.ArrayList;

import cmd.CheckMethodChanges;
import interfaces.InterfaceSQLCredentials;

public class MapEffortToMethod extends SqlCredintials {

	public static void mapEffortToMethods(String projectName, String proxyName)
			throws SQLException, IOException {

		Connection connection = DriverManager.getConnection(url, username,
				password);

		String query;
		if (proxyName == null) {
			query = "SELECT m.ID, m.MethodName, m.CommitId, COUNT(Distinct m.`MethodPath`) JavaFiles, COUNT(m.MethodPath) Methods, i.TimeEffort, p.NumberOfFiles "
					+ "FROM `methodsdetails` as m, `issuesandcommits` as i, `proxys` as p "
					+ "WHERE m.`Changed` = 1 "
					+ "AND m.`ProjectName` = '"
					+ projectName
					+ "' "
					+ "AND m.`CommitId` = i.`CommitId` "
					+ "AND m.`CommitId` = p.`commitId` "
					+ "GROUP BY m.CommitId ";
		} else
			query = getProxyNameQuery(projectName, proxyName);

		// create the java statement
		Statement st = connection.createStatement();

		// execute the query, and get a java resultset
		ResultSet rs = st.executeQuery(query);
		String file = null;
		ArrayList<Integer> linesOfCodeChanged = null;
		// iterate through the java resultset
		while (rs.next()) {
			// get results from the table
			int id = rs.getInt("ID");
			String methodName = rs.getString("MethodName");
			String commitId = rs.getString("CommitId");
			int numberOfFiles = rs.getInt("NumberOfFiles");
			int timeEffort = rs.getInt("TimeEffort");
			int numberOfJavaFiles = rs.getInt("JavaFiles");
			int numberOfMethods = rs.getInt("Methods");

			// update the method as changed in the database

			double effortPerMethod = (((double) timeEffort / numberOfFiles) * numberOfJavaFiles)
					/ numberOfMethods;
			int cEffortPerMethod = (int) Math.ceil(effortPerMethod);
			System.out.println("Method: " + methodName
					+ " has been updated effort of: " + cEffortPerMethod);
			if (proxyName == null)
				updateChanges(commitId, cEffortPerMethod, connection);
			else
				updateChangesForProxy(projectName, id, cEffortPerMethod,
						proxyName, connection);
		}
		connection.close();
	}

	private static void updateChanges(String commitId, int EffortPerMethod,
			Connection connection) throws SQLException {
		String insertQuery1 = "UPDATE  methodsdetails SET TimeSpent = ?  "
				+ "WHERE CommitID LIKE '" + commitId + "' ";

		PreparedStatement preparedStatement1 = connection
				.prepareStatement(insertQuery1);
		preparedStatement1.setInt(1, EffortPerMethod);// int
		preparedStatement1.executeUpdate();
	}

	private static void updateChangesForProxy(String projectName, int methodId,
			int EffortPerMethod, String proxyName, Connection connection)
			throws SQLException {
		String insertQuery1 = "INSERT INTO  effort_per_method "
				+ " (ProjectName, ProxyName, TimeSpent, MethodId)"
				+ "VALUES (?, ?, ?, ?)";

		PreparedStatement preparedStatement1 = connection
				.prepareStatement(insertQuery1);
		preparedStatement1.setString(1, projectName);
		preparedStatement1.setString(2, proxyName);
		preparedStatement1.setInt(3, EffortPerMethod);
		preparedStatement1.setInt(4, methodId);// int
		preparedStatement1.executeUpdate();
	}

	private static String getProxyNameQuery(String projectName, String proxyName) {
		String query = "SELECT m.*, COUNT(*) nrOfMethods, i." + proxyName
				+ " as TimeEffort, p.commitId, p.NumberOfFiles "
				+ "FROM `methodsdetails` as m, `effort` as i, `proxys` as p "
				+ "WHERE m.`Changed` = 1 " + "AND m.`ProjectName` = '"
				+ projectName + "' " + "AND m.`CommitId` = i.`commitId` "
				+ "AND m.`CommitId` = p.`commitId` "
				+ "GROUP BY m.MethodPath, m.`CommitId` ";

		return query;

	}

}
