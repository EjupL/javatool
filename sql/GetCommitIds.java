package sql;

import interfaces.InterfaceSQLCredentials;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class GetCommitIds extends SqlCredintials {

	private List<String> commitIDList = new ArrayList<String>();

	public List<String> getCommitIDList() {
		return commitIDList;
	}

	public void getCommitId(String projectName, String projectVersion)
			throws SQLException {

		Connection connection = DriverManager.getConnection(url, username,
				password);

		String query = "SELECT CommitId FROM commitid WHERE ProjectVersion = "
				+ projectVersion + " AND ProjectName = \"" + projectName + "\"";
		// create the java statement
		Statement st = connection.createStatement();

		// execute the query, and get a java resultset
		ResultSet rs = st.executeQuery(query);
		while (rs.next()) {
			String commitId = rs.getString("CommitId");

			System.out.println(commitId);
			commitIDList.add(commitId);
		}
		connection.close();
	}
}
