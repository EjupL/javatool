package sql;

import interfaces.InterfaceSQLCredentials;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import cmd.ExtractCommits;

public class InsertCommitDetails extends SqlCredintials {

	public void insertCommitList(ExtractCommits commits) throws SQLException {

		Connection connection = DriverManager.getConnection(url, username,
				password);

		String insertQuery = "INSERT INTO commitid (CommitId, projectURL, ProjectName) VALUES (?, ?, ?)";

		PreparedStatement ps = connection.prepareStatement(insertQuery);

		for (String commitID : commits.getCommitId()) {
			ps.setString(1, commitID);
			ps.setString(2, commits.getcommitIdLink().get(commitID));
			ps.setString(3, commits.getProjectName());

			try {
				ps.executeUpdate();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				continue;
			}
		}

		connection.close();

	}

}
