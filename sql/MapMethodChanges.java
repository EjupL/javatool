package sql;

import interfaces.InterfaceSQLCredentials;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Map;

import cmd.CheckMethodChanges;
import cmd.ExtractCommits;

public class MapMethodChanges extends SqlCredintials {

	public static void updateMethodThatChanged(String projectPath,
			String projectName, ExtractCommits projectDetails)
			throws SQLException, IOException, ParseException {

		Connection connection = DriverManager.getConnection(url, username,
				password);

		String query = "SELECT ID, MethodName, MethodLineStart, MethodLineEnd, MethodPath, CommitId FROM methodsdetails WHERE ProjectName = '"
				+ projectName + "'";
		// create the java statement
		Statement st = connection.createStatement();

		// execute the query, and get a java resultset
		ResultSet rs = st.executeQuery(query);
		String file = null;
		ArrayList<Integer> linesOfCodeChanged = null;
		// iterate through the java resultset
		while (rs.next()) {
			// get results from the table
			int id = rs.getInt("ID");
			String methodName = rs.getString("MethodName");
			int startLine = Integer.parseInt(rs.getString("MethodLineStart"));
			int endLine = Integer.parseInt(rs.getString("MethodLineEnd"));
			String methodPath = rs.getString("MethodPath");
			String commitId = rs.getString("CommitId");
			// check if the file is null and if the file is the same as previous
			// that was compared
			// CheckMethodChanges returns an arraylist with integer representing
			// lines of code that changed
			if (file == null || !file.equals(methodPath)) {
				file = methodPath;
				CheckMethodChanges methods = new CheckMethodChanges();

				if (projectDetails != null)
					linesOfCodeChanged = methods.getLinesThatChanged(
							projectPath
									+ projectDetails.getcommitIdAndGitDirMAP()
											.get(commitId), file, commitId);
				else
					linesOfCodeChanged = methods.getLinesThatChanged(
							projectPath, file, commitId);

			}

			if (linesOfCodeChanged.size() == 0)
				continue;

			if (startLine > linesOfCodeChanged
					.get(linesOfCodeChanged.size() - 1))
				continue;

			if (endLine < linesOfCodeChanged.get(0))
				continue;

			for (int i = 0; i < linesOfCodeChanged.size(); i++) {

				if (linesOfCodeChanged.get(i) >= startLine
						&& linesOfCodeChanged.get(i) <= endLine) {
					System.out.println(methodName + " is changed: TRUE");
					// update the method as changed in the database
					updateChanges(id, commitId, connection);
				}

			}

		}
		connection.close();
	}

	private static void updateChanges(int id, String commitId,
			Connection connection) throws SQLException {
		String insertQuery1 = "UPDATE  methodsdetails SET Changed = ?  WHERE ID = "
				+ id + " ";

		PreparedStatement preparedStatement1 = connection
				.prepareStatement(insertQuery1);
		preparedStatement1.setInt(1, 1);// int
		preparedStatement1.executeUpdate();
	}
}
