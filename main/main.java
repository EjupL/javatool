package main;

import java.util.Map;

import namingConvention.CheckMethodsNames;

import org.apache.commons.io.FilenameUtils;

import proxyManager.ExtractProxys;
import cmd.*;
import sql.InsertCommitDetails;
import sql.MapEffortToMethod;
import sql.MapMethodChanges;
import sql.sqlManager;
import sql.sqlRegressionModel;
import lawOfDemeter.AnalyzeMethodsByLawOfDemeter;
public class main {

	@SuppressWarnings("static-access")
	public static void main(String[] args) throws Exception {

		RunCmd command = new RunCmd();

		long startTime = System.currentTimeMillis();
		sqlManager.rundDriver();



		String hibernateProjectPath = "D:\\hibtest\\";
		//String JBossProjectPath = "D:\\jBossTest\\";
		//String SpringProjectPath = "D:\\SpringTest\\";
		String hibernateURL = "https://api.github.com/repos/hibernate/hibernate-orm/commits/";
		

		ExtractCommits projectAnalyzed = new ExtractCommits("Spring-JIRA", hibernateURL);
		// hibernate.extractCommitsForProject(hibernateProjectPath, dateFrom,
		//		dateUntil);

		projectAnalyzed.extractCommitsFromJIRAProject("Hibernate");

		 InsertCommitDetails insertCommits = new InsertCommitDetails();
		insertCommits.insertCommitList(projectAnalyzed);
		        int c = 0;
		        int end = projectAnalyzed.getCommitId().size();
		        int returnedNULL = 0;
		        for (String commitID : projectAnalyzed.getCommitId()) {
		        	if(projectAnalyzed.commitExist(commitID)){
		        		c++;
		        		continue;
		        	}
		        	
		        	
		        	ExtractProxys proxy = new ExtractProxys();
		        	Map<String, Integer> results = proxy.extraxtProxys(
		        			projectAnalyzed.getcommitIdLink().get(commitID), commitID);
		        	if(proxy == null){returnedNULL++; continue;} 
		        	
		        	System.out.println(results.get("longestPrefix"));
		        	System.out.println(results.get("additions"));
		        	System.out.println(results.get("extensions"));
		        	System.out.println(results.get("numberOfFiles"));
		        	System.out.println("---------------------------");
		        	
		        	if (results.get("numberOfFiles") < 100) {
		        		sqlManager.insertProxys(results.get("additions"),
		        				results.get("extensions"),
		        				results.get("longestPrefix"),
		        				results.get("numberOfFiles"), 
		        				commitID);
		        		
		        		System.out.println(c++ + " out of " + end);
		        		System.out.println("Returned null: " + returnedNULL);
		        		
		        		for (String aFilePath : proxy.getFilesChanged()) {
		        			proxy.getFilesStream().get(aFilePath);
		        			// Check if the file is Java file. Then extract methods and
		        			// comments. And insert them to database.
		        			if (FilenameUtils.getExtension(aFilePath).equals("java"))
		        				command.runCmdCommand(projectAnalyzed.getProjectName(), hibernateProjectPath, aFilePath, commitID, true);
		        		}
		        	}
		        
		
		        }

		/**
		 * Project Details needs to be null for projects that are not extracted from JIRA
		 */
		
	    MapMethodChanges.updateMethodThatChanged("D:\\Hibernate\\", "Hibernate", projectAnalyzed);
	    //MapMethodChanges.updateMethodThatChanged("D:\\Spring\\", "Spring", projectDetails);
	    //MapMethodChanges.updateMethodThatChanged("D:\\JBoss\\", "JBoss", projectDetails);
		
	    /**
		 * Project Details needs to be null for projects that are not extracted from JIRA
		 */
		/*//ExtractCommits projectDetails = null;
		 * MapEffortToMethod.mapEffortToMethods("Hibernate-JIRA", null);
		MapEffortToMethod.mapEffortToMethods("Spring-JIRA", null);
		MapEffortToMethod.mapEffortToMethods("JBoss-JIRA", null);*/
		
		/**
		 * Identify which methods follow the Concise Naming Convention Good Practice
		 */
		CheckMethodsNames hibernateMethods = new CheckMethodsNames();
		//CheckMethodsNames jBossMethods = new CheckMethodsNames();
		//CheckMethodsNames springMethods = new CheckMethodsNames();

		hibernateMethods.analyzeMethodsForProject("Hibernate-JIRA");
		//jBossMethods.analyzeMethodsForProject("JBoss-JIRA");
		//springMethods.analyzeMethodsForProject("Spring-JIRA");

		/**
		 * Identify methods that follow the law of demeter and those that does not follow.
		 */
		AnalyzeMethodsByLawOfDemeter.analyseMethodsForProject("Hibernate-JIRA");
		//AnalyzeMethodsByLawOfDemeter.analyseMethodsForProject("Spring-JIRA");
	   // AnalyzeMethodsByLawOfDemeter.analyseMethodsForProject("JBoss-JIRA");
		

		long endTime = System.currentTimeMillis();
		System.out.println("That took " + (endTime - startTime)
				+ " milliseconds" + " or " + (endTime - startTime) / 1000
				+ " seconds!");

	} 


}
