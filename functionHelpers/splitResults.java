package functionHelpers;

import java.util.HashMap;
import java.util.Map;

public class splitResults {

	public static Map<String, String> splitMethodExtractor(String result) {
		Map<String, String> finalResults = new HashMap<String, String>();

		String arrayString[] = result.split("\\s+");
		finalResults.put("path",
				arrayString[0].replace("\\", "\\\\").replace("/", "\\\\"));
		finalResults.put("method", arrayString[1]);
		finalResults.put("methodName", getMethodName(arrayString[1]));
		finalResults.put("methodType", getMethodType(arrayString[1]));
		finalResults.put("startLine", arrayString[2]);
		finalResults.put("endLine", arrayString[3]);

		return finalResults;
	}

	public static String getMethodName(String line) {
		// Split the string based on given characters;
		String arrayString[] = line.split("[# :]");

		if (arrayString[1].startsWith("|") || !arrayString[1].endsWith(")"))
			return arrayString[2];
		else
			return arrayString[1];
	}

	public static String getMethodType(String line) {
		String arrayString[] = line.split("[# :]");
		String methodType = arrayString[2];

		return methodType;
	}

	/**
	 * This method split's the countResult by identifying the white spaces Then
	 * save values to Map data structure, type number as key and number of
	 * comments as value
	 * 
	 * @param countResult
	 *            e.g. "2 0 3 6 0 0 0"
	 * @return Map<String, Integer> e.g. {1=2, 2=0, 3=3, 4=6, ...etc.}
	 */
	public static Map<String, Integer> splitCommentCounter(String countResult) {
		Map<String, Integer> finalResults = new HashMap<String, Integer>();
		String arrayString[] = countResult.split("\\s+");

		finalResults.put("1", Integer.parseInt(arrayString[0]));
		finalResults.put("2", Integer.parseInt(arrayString[1]));
		finalResults.put("3", Integer.parseInt(arrayString[2]));
		finalResults.put("4", Integer.parseInt(arrayString[3]));
		finalResults.put("5", Integer.parseInt(arrayString[4]));
		finalResults.put("6", Integer.parseInt(arrayString[5]));
		finalResults.put("7", Integer.parseInt(arrayString[5]));

		return finalResults;
	}

}
