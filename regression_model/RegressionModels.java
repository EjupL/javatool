package regression_model;

public class RegressionModels {

	private double b0;
	private double b1;
	private double y;

	public RegressionModels(double b0, double b1) {
		this.b0 = b0;
		this.b1 = b1;

	}

	public double calculateLinearModel(int aValue) {
		return (calculateFormula(aValue));
	}

	private double calculateFormula(int aValue) {
		this.y = this.b0 + (this.b1 * aValue);

		return this.y;
	}

}
