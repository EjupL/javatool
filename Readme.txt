/**Date: JUNE 2015**/
/**Author: Ejup Ljatifi, student at VUB**//
/****VRIJE UNIVERSITEIT BRUSSEL***///
/****FACULTY OF SCIENCE AND BIO-ENGINEERING SCIENCES Department of Computer Science****/
Script developed to gather dataset for statistical analyses that were conducted for
Graduation thesis submitted in partial fulillment of the requirements for the degree 
of Master of Science in Applied Sciences and Engineering: Computer Science.

We want to document the set of tools developed to collect all datasets that were necessary to
 perform the analyzes, which were designed to be extensible so that similar studies on Good
 Practices can be performed using the same set of tools. The set of tools that we integrated
 together with our tool are JavaMethodExtractor and CommentCounter developed by Aman et al. 
[1]; and, INTT (Identiﬁer Name Tokenisation Tool) developed by Butler et al. [2]

/**INTT is Copyright (C) 2011 The Open University and is licensed for use under the conditions
 that are documented in their ZIP archive: http://oro.open.ac.uk/28352/**/

[1] Hirohisa Aman, Sousuke Amasaki, Takashi Sasaki, and Minoru Kawahara. Empir- ical analysis
 of fault-proneness in methods by focusing on their comment lines. In 21st Asia-Paciﬁc Software
 Engineering Conference, APSEC 2014, Jeju, South Ko- rea, December 1-4, 2014. Volume 2: 
Industry, Short, and QuASoQ Papers, pages 51–56, 2014.

[2] Simon Butler, Michel Wermelinger, Yijun Yu, and Helen Sharp. Improving the tokenisation of 
identiﬁer names. In ECOOP 2011 - Object-Oriented Programming - 25th European Conference, 
Lancaster, UK, July 25-29, 2011 Proceedings, pages 130–154, 2011.



**The main steps that need to be taken into consideration before using the tool are:**

1. In order to be able to make secure URL requests to the Version Control System (i.e. GitHub),
 in the package urlParses the class JsonReader.java needs to be provided with the username and
 password of Version Control System (i.e. GitHub) account.
2. It is necessary to fill the database credentials (i.e. url, username, password) connec- tion
 that are inside the interface InterfaceSQLCredentials.java on package called interfaces .
3. If database schema is changed, then it is necessary to check all methods that perform SQL 
query to the database.


The dataset can be found together with the database structure in the JavaToolDatabase.sql in the root folder.

Information of the PHP script can be found in the following link: https://bitbucket.org/EjupL/php_script